# validationScripts

Each folder has a scripts or set of scripts to run on a MuonTester ouput and produce some figures.

- `asbuilt`: process one quad at a time and plot the shift of a layer with respect to a reference layer
- `caruanaTest`: compare the cluster size, track residual and track pull between the caruana method and weighted average
- `channelNumber`: a few scripts to verify the channel numbering
- `checkCluster`: plot the average strip cluster size as a function of theta
- `resolution`: plot the standard deviation of the residual distribution as a function of theta, and a script to compute the resolution using the inclusive and exclusive sigmas. Note the computation of the inclusive and exclusive sigmas are not correct, since all quads are combined while the correct way is to carry out the calculations for each quad separately.
- `trackResidual`: verify the on-track residuals obtained directly from MuonTester
