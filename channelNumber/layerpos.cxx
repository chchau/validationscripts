
#include <cstdint>

enum ChannelType{PAD=0, STRIP=1, WIRE=2};

struct Key {
  //uint32_t m_id{0};
  int m_sector{0};
  int m_stEta{0};
  int m_stPhi{0};
  int m_mlt{0};
  int m_layer{0};
  int m_chType{0};
  int m_ch{0};

  Key () {}
  Key(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch)
    : m_sector(stIndex), m_stEta(stEta), m_stPhi(stPhi), m_mlt(mlt), m_layer(gap), m_chType(chtype), m_ch(ch)
  {
  }

  int stationIndex() const {return m_sector;}
  int stationEta() const {return m_stEta;}
  int stationPhi() const {return m_stPhi;}
  int multiplet() const {return m_mlt;}
  int layer() const {return m_layer;}
  int channelType() const {return m_chType;}
  int channel() const {return m_ch;}

  bool isSameKey(const Key& k) {
    if (k.stationIndex() == m_sector && k.stationEta() == m_stEta && k.stationPhi() == m_stPhi &&
        k.multiplet() == m_mlt && k.layer() == m_layer && k.channelType() == m_chType && k.channel() == m_ch) {
      return true;
    }
    return false;
  }
};

struct Channel {
  Key m_key;
  int m_channel{0}, m_channelType{-1};
  double m_localPosX{0.}, m_localPosY{0.};
  double m_globalPosX{0.}, m_globalPosY{0.}, m_globalPosZ{0.};
  // Corners 1 and 2 are also associated to the width of strip and pad,
  // while corners 3 and 4 are associated to the length.
  double m_corner1{0.}, m_corner2{0.}, m_corner3{0.}, m_corner4{0.};

  Channel(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch, 
          double lposX, double lposY, double gposX, double gposY, double gposZ) 
      : m_key(Key(stIndex, stEta, stPhi, mlt, gap, chtype, ch)),
        m_channelType(chtype),
        m_channel(ch),
        m_localPosX(lposX),
        m_localPosY(lposY),
        m_globalPosX(gposX),
        m_globalPosY(gposY),
        m_globalPosZ(gposZ)
  {
  }

  void insertCorners(double c1, double c2, double c3, double c4) {
    m_corner1 = c1;
    m_corner2 = c2;
    m_corner3 = c3;
    m_corner4 = c4;
  }

  int insertWidthLength(double width, double length) {
    if (m_channelType == ChannelType::PAD) {
        std::cout << "insertWidth does not support pad" << std::endl;
        return 1;
    }
    m_corner1 = m_localPosX - width/2;
    m_corner2 = m_localPosX + width/2;
    m_corner3 = m_localPosY - length/2;
    m_corner4 = m_localPosY + length/2;
    return 0;
  }

  double width() const {
    return m_corner2 - m_corner1;
  }

  double length() const {
    return m_corner4 - m_corner3;
  }
};

std::vector<Channel> retrieveChamber(std::vector<Channel>& stgcChannels, int stIndex, int stEta, int stPhi,
                                     int mlt, int gap, int chType) {
  std::vector<Channel> outChamber;
  for (Channel& ch: stgcChannels) {
    if (ch.m_key.stationIndex()==stIndex && ch.m_key.stationEta()==stEta && ch.m_key.stationPhi()==stPhi && 
        ch.m_key.multiplet()==mlt && ch.m_key.layer()==gap && ch.m_key.channelType()==chType) {
      bool duplicate{false};
      for (const Channel& c: outChamber) {
        if (ch.m_key.channel() == c.m_key.channel()) {
          duplicate = true;
          break;
        }
      }
      if (!duplicate) outChamber.push_back(ch);
    }
  }

  return std::move(outChamber);
}

int plotChannels(std::vector<Channel>& stgcChannels, int channel_type) {
  int stationIndex[2] = {57,58};
  int stationEta[6] = {-3,-2,-1,1,2,3};
  int stationPhi[8] = {1,2,3,4,5,6,7,8};
  int multiplet[2] = {1,2};
  int layer[4] = {1,2,3,4};
  //int channelType[3] = {0,1,2};

  bool enablePad = (channel_type == 0);
  bool enableStrip = (channel_type == 1);
  bool enableWire = (channel_type == 2);

  // output folder
  //TString outputFolder = "plots_mc/";
  TString outputFolder = "plots_datA/";

  // Retrieve the channels of a chamber
  for (int stIndex: stationIndex) {
    for (int stEta: stationEta) {
      for (int stPhi: stationPhi) {
        for (int mlt: multiplet) {
          for (int lay: layer) {

            // Plot pads
            int chType = 0;
            if (enablePad) {
              std::vector<Channel> padChannels = retrieveChamber(stgcChannels, stIndex, stEta, stPhi, mlt, lay, chType);
              int nChannel = static_cast<int>(padChannels.size());
              if (nChannel < 1) continue; //skip plotting empty graph
              float xpos[nChannel];
              float ypos[nChannel];
              int ch[nChannel];
              int chMin{999}, chMax{-999};
              float chMinPos[2] = {0.,0.};
              float chMaxPos[2] = {0.,0.};
              for (int i = 0; i < nChannel; ++i) {
                const Channel& c = padChannels.at(i);
                int tmp_ch = c.m_channel;
                ch[i] = c.m_channel;
                if (tmp_ch < chMin) { 
                  chMin = tmp_ch;
                  chMinPos[0] = c.m_globalPosX;
                  chMinPos[1] = c.m_globalPosY;
                }
                if (tmp_ch > chMax) {
                  chMax = tmp_ch;
                  chMaxPos[0] = c.m_globalPosX;
                  chMaxPos[1] = c.m_globalPosY;
                }
                xpos[i] = c.m_globalPosX;
                ypos[i] = c.m_globalPosY;
              }
              TCanvas c1("c1", "canvas", 800, 600);
              TGraph h_pad(nChannel, xpos, ypos);
              h_pad.SetMarkerStyle(20);
              h_pad.SetMarkerSize(0.6);
              h_pad.Draw("AP");
              // draw text
              TText t;
              t.SetTextFont(42);
              t.SetTextSize(0.02);
              char val[4];
              float offsetX = std::abs(chMinPos[0] - chMaxPos[0]) * 0.02;
              float offsetY = std::abs(chMinPos[1] - chMaxPos[1]) * 0.02;
              if ((chMinPos[0]-chMaxPos[0]) * (chMinPos[1] - chMaxPos[1]) > 0) {
                offsetX *= -1;
              } else if (std::abs(chMinPos[0]-chMaxPos[0]) < 0.1) {
                offsetY *= -1;
              } else if (std::abs(chMinPos[1]-chMaxPos[1]) < 0.1) {
                offsetX *= -1;
              }

              t.DrawText(chMinPos[0] + offsetX, chMinPos[1] + offsetY, TString::Format("%d",chMin));
              for (int i = 0; i < nChannel; ++i) {
                //if ((ch[i] % 10) == 0)
                  t.DrawText(xpos[i] + offsetX, ypos[i] + offsetY, TString::Format("%d",ch[i]));
              }
              t.DrawText(chMaxPos[0] + offsetX, chMaxPos[1] + offsetY, TString::Format("%d",chMax));
              TString hname = outputFolder + "pad_" + TString::Format("%d_%d_%d_%d_%d", stIndex, stEta, stPhi, mlt, lay);
              c1.Print(hname+".pdf");
            }

            // Plot strips
            chType = 1;
            if (enableStrip) {
              std::vector<Channel> stripChannels = retrieveChamber(stgcChannels, stIndex, stEta, stPhi, mlt, lay, chType);
              // Find the minimum and maximum positions
              int nChannel = static_cast<int>(stripChannels.size());
              if (nChannel < 1) continue; //skip plotting empty graph
              float xpos[nChannel];
              float ypos[nChannel];
              int ch[nChannel];
              int chMin{999}, chMax{-999};
              float chMinPos[2] = {0.,0.};
              float chMaxPos[2] = {0.,0.};
              for (int i = 0; i < nChannel; ++i) {
                const Channel& c = stripChannels.at(i);
                int tmp_ch = c.m_channel;
                ch[i] = c.m_channel;
                if (tmp_ch < chMin) { 
                  chMin = tmp_ch;
                  chMinPos[0] = c.m_globalPosX;
                  chMinPos[1] = c.m_globalPosY;
                }
                if (tmp_ch > chMax) {
                  chMax = tmp_ch;
                  chMaxPos[0] = c.m_globalPosX;
                  chMaxPos[1] = c.m_globalPosY;
                }
                xpos[i] = c.m_globalPosX;
                ypos[i] = c.m_globalPosY;
              }
              TCanvas c1("c1", "canvas", 800, 600);
              TGraph h_strip(nChannel, xpos, ypos);
              h_strip.SetMarkerStyle(20);
              h_strip.SetMarkerSize(0.4);
              h_strip.Draw("AP");
              // draw text
              TText t;
              t.SetTextFont(42);
              t.SetTextSize(0.01);
              char val[4];
              float offsetX = std::abs(chMinPos[0] - chMaxPos[0]) * 0.01;
              float offsetY = std::abs(chMinPos[1] - chMaxPos[1]) * 0.01;
              if ((chMinPos[0]-chMaxPos[0]) * (chMinPos[1] - chMaxPos[1]) > 0) {
                offsetX *= -1;
              } else if (std::abs(chMinPos[0]-chMaxPos[0]) < 0.1) {
                offsetY *= -1;
              } else if (std::abs(chMinPos[1]-chMaxPos[1]) < 0.1) {
                offsetX *= -1;
              }

              t.DrawText(chMinPos[0] + offsetX, chMinPos[1] + offsetY, TString::Format("%d",chMin));
              for (int i = 0; i < nChannel; ++i) {
                if ((ch[i] % 10) == 0)
                  t.DrawText(xpos[i] + offsetX, ypos[i] + offsetY, TString::Format("%d",ch[i]));
              }
              t.DrawText(chMaxPos[0] + offsetX, chMaxPos[1] + offsetY, TString::Format("%d",chMax));
              TString hname = outputFolder + "strip_" + TString::Format("%d_%d_%d_%d_%d", stIndex, stEta, stPhi, mlt, lay);
              c1.Print(hname+".pdf");
            }

            // Plot wiregroups
            chType = 2;
            if (enableWire) {
              std::vector<Channel> wireChannels = retrieveChamber(stgcChannels, stIndex, stEta, stPhi, mlt, lay, chType);
              // Find the minimum and maximum positions
              int nChannel = static_cast<int>(wireChannels.size());
              if (nChannel < 1) continue; //skip plotting empty graph
              float xpos[nChannel];
              float ypos[nChannel];
              int ch[nChannel];
              int chMin{999}, chMax{-999};
              float chMinPos[2] = {0.,0.};
              float chMaxPos[2] = {0.,0.};
              for (int i = 0; i < nChannel; ++i) {
                const Channel& c = wireChannels.at(i);
                int tmp_ch = c.m_channel;
                ch[i] = c.m_channel;
                if (tmp_ch < chMin) {
                  chMin = tmp_ch;
                  chMinPos[0] = c.m_globalPosX;
                  chMinPos[1] = c.m_globalPosY;
                }
                if (tmp_ch > chMax) {
                  chMax = tmp_ch;
                  chMaxPos[0] = c.m_globalPosX;
                  chMaxPos[1] = c.m_globalPosY;
                }
                xpos[i] = c.m_globalPosX;
                ypos[i] = c.m_globalPosY;
              }
              TCanvas c1("c1", "canvas", 800, 600);
              TGraph h_wire(nChannel, xpos, ypos);
              h_wire.SetMarkerStyle(20);
              h_wire.SetMarkerSize(0.6);
              h_wire.Draw("AP");
              // draw text
              TText t;
              t.SetTextFont(42);
              t.SetTextSize(0.02);
              char val[4];
              float offsetX = std::abs(chMinPos[0] - chMaxPos[0]) * 0.02;
              float offsetY = std::abs(chMinPos[1] - chMaxPos[1]) * 0.02;
              if ((chMinPos[0]-chMaxPos[0]) * (chMinPos[1] - chMaxPos[1]) > 0) {
                offsetX *= -1;
              } else if (std::abs(chMinPos[0]-chMaxPos[0]) < 0.1) {
                offsetY *= -1;
              } else if (std::abs(chMinPos[1]-chMaxPos[1]) < 0.1) {
                offsetX *= -1;
              }

              t.DrawText(chMinPos[0] + offsetX, chMinPos[1] + offsetY, TString::Format("%d",chMin));
              for (int i = 0; i < nChannel; ++i) {
                if ((ch[i] % 5) == 0)
                  t.DrawText(xpos[i] + offsetX, ypos[i] + offsetY, TString::Format("%d",ch[i]));
              }
              t.DrawText(chMaxPos[0] + offsetX, chMaxPos[1] + offsetY, TString::Format("%d",chMax));
              TString hname = outputFolder + "wire_" + TString::Format("%d_%d_%d_%d_%d", stIndex, stEta, stPhi, mlt, lay);
              c1.Print(hname+".pdf");
            }

          }
        }
      }
    }
  }

  // Plot the global positions (x,y) and channel number

  return 0;
}

int layerpos () {

  const double HQ1 = 0.0;
  const double HQ2 = 1320.0; // in mm
  const double HQ3 = 1200.0; // in mm

  // Plotting style
  gStyle->SetTitleYOffset(1.2);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  //gStyle->SetOptStat(0);

  std::cout << "... macro starting" << std::endl;
  TTreeReader reader;
  TChain* fchain = new TChain("BasicTesterTree");
  fchain->Add("stgc_431914_lb0136_sf012_a.root");
  fchain->Add("stgc_431914_lb0795_sf014_a.root");
  reader.SetTree(fchain);

  TTreeReaderArray<unsigned short> stgc_channel      (reader, "stgc_channel");
  TTreeReaderArray<vector<unsigned short>> stgcStripChannel      (reader, "stgcStripChannel");
  TTreeReaderArray<unsigned char> stgc_channel_type      (reader, "stgc_channel_type");
  TTreeReaderArray<unsigned char> stgc_gas_gap      (reader, "stgc_gas_gap");
  TTreeReaderArray<unsigned char> stgc_multiplet      (reader, "stgc_multiplet");
  TTreeReaderArray<unsigned char> stgc_stationPhi      (reader, "stgc_stationPhi");
  TTreeReaderArray<char> stgc_stationEta      (reader, "stgc_stationEta");
  TTreeReaderArray<unsigned char> stgc_stationIndex      (reader, "stgc_stationIndex");
  TTreeReaderArray<float> stgcGlobalPos_z      (reader, "stgcGlobalPos_z");
  TTreeReaderArray<float> stgcGlobalPos_x      (reader, "stgcGlobalPos_x");
  TTreeReaderArray<float> stgcGlobalPos_y      (reader, "stgcGlobalPos_y");
  TTreeReaderArray<float> stgcLocalPos_x      (reader, "stgcLocalPos_x");
  TTreeReaderArray<float> stgcLocalPos_y      (reader, "stgcLocalPos_y");

  std::cout  << "...starting loop" << std::endl;
  int count{0};
  std::vector<Channel> stgcPads;
  std::vector<Channel> stgcStrips;
  std::vector<Channel> stgcWiregroups;
  while (reader.Next()) {
    //if ((count/100) == 0) std::cout << "Entries " << count << std::endl;
    //if (count > 500) break;
    ++count;

    int nHit = stgc_channel.GetSize();
    for (int i = 0; i < nHit; ++i) {
      int stationIndex = static_cast<int>(stgc_stationIndex[i]);
      int stationEta = static_cast<int>(stgc_stationEta[i]);
      int stationPhi = static_cast<int>(stgc_stationPhi[i]);
      int multiplet = static_cast<int>(stgc_multiplet[i]);
      int layer = static_cast<int>(stgc_gas_gap[i]);
      int channelType = static_cast<int>(stgc_channel_type[i]);
      int chNumber = static_cast<int>(stgc_channel[i]);

      Channel channel(stationIndex, stationEta, stationPhi, multiplet, layer, channelType, chNumber,
                      stgcLocalPos_x[i], stgcLocalPos_y[i],
                      stgcGlobalPos_x[i], stgcGlobalPos_y[i], stgcGlobalPos_z[i]);

      std::vector<Channel>& stgcChannels = (channelType == ChannelType::PAD)? stgcPads:
                                           (channelType == ChannelType::STRIP)? stgcStrips: stgcWiregroups;
      if ((i < 10) && (count < 2)){
        std::cout << "Count: " << count
                  << " " << static_cast<int>(stgc_stationIndex[i])
                  << " " << static_cast<int>(stgc_stationEta[i])
                  << " " << static_cast<int>(stgc_stationPhi[i])
                  << " " << static_cast<int>(stgc_multiplet[i])
                  << " " << static_cast<int>(stgc_gas_gap[i])
                  << " " << static_cast<int>(stgc_channel_type[i])
                  << " " << static_cast<int>(stgc_channel[i])
                  << std::endl;
      }

      bool foundHit{false};
      for (Channel& ch: stgcChannels) {
        if (ch.m_key.isSameKey(channel.m_key)) {
          // Found existing readout channel
          foundHit = true;
          break;
        }
      }
      if (!foundHit) {
        stgcChannels.push_back(channel);
      }
    }
  }
  plotChannels(stgcPads, 0);
  plotChannels(stgcStrips, 1);
  plotChannels(stgcWiregroups, 2);

  return 0;
}
