
struct Key {
  int m_sector{0};
  int m_stEta{0};
  int m_stPhi{0};
  int m_mlt{0};
  int m_layer{0};
  int m_chType{0};
  int m_ch{0};

  Key () {}
  Key(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch)
    : m_sector(stIndex), m_stEta(stEta), m_stPhi(stPhi), m_mlt(mlt), m_layer(gap), m_chType(chtype), m_ch(ch)
  {
  }

  int stationIndex() const {return m_sector;}
  int stationEta() const {return m_stEta;}
  int stationPhi() const {return m_stPhi;}
  int multiplet() const {return m_mlt;}
  int layer() const {return m_layer;}
  int channelType() const {return m_chType;}
  int channel() const {return m_ch;}

  bool isSameKey(const Key& k) {
    if (k.stationIndex() == m_sector && k.stationEta() == m_stEta && k.stationPhi() == m_stPhi &&
        k.multiplet() == m_mlt && k.layer() == m_layer && k.channelType() == m_chType && k.channel() == m_ch) {
      return true;
    }
    return false;
  }
};

struct point2D {
  double x{0.};
  double y{0.};
  
  point2D(): x(0.), y(0.) {}
  point2D(double a, double b): x(a), y(b) {}
};

struct Channel {
  Key m_key;
  int m_channel{0}, m_channelType{-1};
  double m_globalPosX{0.}, m_globalPosY{0.}, m_globalPosZ{0.};
  point2D m_corner1, m_corner2, m_corner3, m_corner4;

  Channel(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch, 
          double gposX, double gposY, double gposZ) 
      : m_key(Key(stIndex, stEta, stPhi, mlt, gap, chtype, ch)),
        m_channelType(chtype),
        m_channel(ch),
        m_globalPosX(gposX),
        m_globalPosY(gposY),
        m_globalPosZ(gposZ)
  {
  }

  void insertCorners(point2D c1, point2D c2, point2D c3, point2D c4) {
    m_corner1 = c1;
    m_corner2 = c2;
    m_corner3 = c3;
    m_corner4 = c4;
  }
};

std::vector<Channel> retrieveChamber(std::vector<Channel>& stgcChannels, int stIndex, int stEta, int stPhi,
                                     int mlt, int gap, int chType) {
  std::vector<Channel> outChamber;
  for (Channel& ch: stgcChannels) {
    if (ch.m_key.stationIndex()==stIndex && ch.m_key.stationEta()==stEta && ch.m_key.stationPhi()==stPhi && 
        ch.m_key.multiplet()==mlt && ch.m_key.layer()==gap && ch.m_key.channelType()==chType) {
      bool duplicate{false};
      for (const Channel& c: outChamber) {
        if (ch.m_key.channel() == c.m_key.channel()) {
          duplicate = true;
          break;
        }
      }
      if (!duplicate) outChamber.push_back(ch);
    }
  }

  return std::move(outChamber);
}

int drawLayout(std::vector<Channel>& stgcChannels) {
  TString outputFolder = "padLayout/";
  const int NCORNER{4};

  if (stgcChannels.empty()) return 0;

  // Identifier
  int sector = stgcChannels[0].m_key.stationIndex();
  int stEta = stgcChannels[0].m_key.stationEta();
  int stPhi = stgcChannels[0].m_key.stationPhi();
  int mlt = stgcChannels[0].m_key.multiplet();
  int layer = stgcChannels[0].m_key.layer();
  char side = (stgcChannels[0].m_key.stationEta() > 0)? 'A': 'C';

  TCanvas c1("c1", "canvas", 600, 800);
  c1.SetRightMargin(0.02);
  c1.SetLeftMargin(0.12);
  c1.SetTopMargin(0.02);
  c1.SetBottomMargin(0.08);
  TH2Poly h_pad;

  // Add corners in the TH2Poly and save the channel position
  std::vector<std::pair<int, point2D>> channelPositions;
  for (Channel& c: stgcChannels) {
    double cornerX[NCORNER] = {c.m_corner1.x, c.m_corner2.x, c.m_corner4.x, c.m_corner3.x};
    double cornerY[NCORNER] = {c.m_corner1.y, c.m_corner2.y, c.m_corner4.y, c.m_corner3.y};
    h_pad.AddBin(NCORNER, cornerX, cornerY);

    // Save pad number and its position
    point2D gpos{c.m_globalPosX, c.m_globalPosY};
    channelPositions.emplace_back(c.m_channel, gpos);
    //std::cout << "corner " << c.m_channel << " " << c.m_globalPosX << c.m_globalPosY << std::endl;
  }

  // Fill histogram
  for (std::pair<int, point2D>& p: channelPositions) {
    double posX{p.second.x}; 
    double posY{p.second.y}; 
    h_pad.Fill(posX, posY, 1.0);
    //std::cout << "fill " << p.first << " " << posX << posY << std::endl;
  }
  
  // Drawing histogram
  h_pad.GetXaxis()->SetRangeUser(-1600., 1600.);
  h_pad.GetXaxis()->SetTitle("Global X");
  h_pad.GetYaxis()->SetRangeUser(900., 4700.);
  h_pad.GetYaxis()->SetTitle("Global Y");
  h_pad.GetYaxis()->SetTitleOffset(1.8);
  h_pad.SetTitle("");
  h_pad.Draw();

  // Draw text
  TText t;
  t.SetTextFont(42);
  t.SetTextSize(0.01);
  for (std::pair<int, point2D>& p: channelPositions) {
    t.DrawText(p.second.x, p.second.y, TString::Format("%d", p.first));
    //std::cout << "txt " << p.first << " " << p.second.x << p.second.y << std::endl;
  }

  TString hname = outputFolder + "layout_" + TString::Format("%d_%c_%d_%d_%d", sector, side, stPhi, mlt, layer);
  c1.Print(hname+".pdf");

  return 0;
}

int plotPadLayout () {

  const double HQ1 = 0.0;
  const double HQ2 = 1320.0; // in mm
  const double HQ3 = 1200.0; // in mm

  // Plotting style
  gStyle->SetTitleYOffset(1.2);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);

  std::cout << "... macro starting" << std::endl;
  TFile* file;
  file = new TFile("r220911/dimuon_stgc.root");
  TTree* tree;
  tree = (TTree*) file->Get("BasicTesterTree");
  TTreeReader reader;
  reader.SetTree(tree);

  TTreeReaderArray<unsigned short> stgc_channel      (reader, "stgc_channel");
  TTreeReaderArray<vector<unsigned short>> stgcStripChannel      (reader, "stgcStripChannel");
  TTreeReaderArray<unsigned char> stgc_channel_type      (reader, "stgc_channel_type");
  TTreeReaderArray<unsigned char> stgc_gas_gap      (reader, "stgc_gas_gap");
  TTreeReaderArray<unsigned char> stgc_multiplet      (reader, "stgc_multiplet");
  TTreeReaderArray<unsigned char> stgc_stationPhi      (reader, "stgc_stationPhi");
  TTreeReaderArray<char> stgc_stationEta      (reader, "stgc_stationEta");
  TTreeReaderArray<unsigned char> stgc_stationIndex      (reader, "stgc_stationIndex");
  TTreeReaderArray<float> stgcGlobalPos_z      (reader, "stgcGlobalPos_z");
  TTreeReaderArray<float> stgcGlobalPos_x      (reader, "stgcGlobalPos_x");
  TTreeReaderArray<float> stgcGlobalPos_y      (reader, "stgcGlobalPos_y");
  TTreeReaderArray<float> stgcLocalPos_x      (reader, "stgcLocalPos_x");
  TTreeReaderArray<float> stgcLocalPos_y      (reader, "stgcLocalPos_y");
  TTreeReaderArray<vector<float>> stgcPadCornersX      (reader, "stgcPadCornersX");
  TTreeReaderArray<vector<float>> stgcPadCornersY      (reader, "stgcPadCornersY");

  std::cout  << "...starting loop" << std::endl;
  int count{0};
  std::vector<Channel> stgcPads;
  while (reader.Next()) {
    //if ((count/100) == 0) std::cout << "Entries " << count << std::endl;
    //if (count > 1000) break;
    ++count;

    int nHit = stgc_channel.GetSize();
    for (int i = 0; i < nHit; ++i) {
      int stationIndex = static_cast<int>(stgc_stationIndex[i]);
      int stationEta = static_cast<int>(stgc_stationEta[i]);
      int stationPhi = static_cast<int>(stgc_stationPhi[i]);
      int multiplet = static_cast<int>(stgc_multiplet[i]);
      int layer = static_cast<int>(stgc_gas_gap[i]);
      int channelType = static_cast<int>(stgc_channel_type[i]);
      int chNumber = static_cast<int>(stgc_channel[i]);

      // Skip non pad hits
      if (channelType != 0) continue;

      Channel channel(stationIndex, stationEta, stationPhi, multiplet, layer, channelType, chNumber,
                      stgcGlobalPos_x[i], stgcGlobalPos_y[i], stgcGlobalPos_z[i]);

      //std::vector<Channel>& stgcChannels = (channelType == ChannelType::PAD)? stgcPads:
      //                                     (channelType == ChannelType::STRIP)? stgcStrips: stgcWiregroups;
      if ((i < 10) && (count < 2)){
        std::cout << "Count: " << count
                  << " " << static_cast<int>(stgc_stationIndex[i])
                  << " " << static_cast<int>(stgc_stationEta[i])
                  << " " << static_cast<int>(stgc_stationPhi[i])
                  << " " << static_cast<int>(stgc_multiplet[i])
                  << " " << static_cast<int>(stgc_gas_gap[i])
                  << " " << static_cast<int>(stgc_channel_type[i])
                  << " " << static_cast<int>(stgc_channel[i])
                  << " " << channel.m_globalPosX << " " << channel.m_globalPosY
                  << std::endl;
      }

      bool foundHit{false};
      for (Channel& ch: stgcPads) {
        if (ch.m_key.isSameKey(channel.m_key)) {
          // Found existing readout channel
          foundHit = true;
          break;
        }
      }
      if (!foundHit) {
        if ((stgcPadCornersX[i].size() != 4) || (stgcPadCornersY[i].size() != 4)) {
          std::cout << "Mis-matched number of pad corners" << std::endl;
          continue;
        }
        channel.insertCorners(point2D(stgcPadCornersX[i][0], stgcPadCornersY[i][0]),
                              point2D(stgcPadCornersX[i][1], stgcPadCornersY[i][1]),
                              point2D(stgcPadCornersX[i][2], stgcPadCornersY[i][2]),
                              point2D(stgcPadCornersX[i][3], stgcPadCornersY[i][3]));
        stgcPads.push_back(channel);
      }
    }
  }

  // Retrive the quad to make plots

  // Create a TH2Poly 
  // Set the corners
  // Draw pad numbers at the center
  // Save plots

  std::vector<int> stationIndex = {58};
  std::vector<int> stationEta = {-2,2};
  std::vector<int> stationPhi = {3};
  std::vector<int> multiplet = {1,2};
  std::vector<int> layer = {1,2,3,4};

  for (int mlt: multiplet) {
    for (int lay: layer) {
      std::vector<Channel> QL1P = retrieveChamber(stgcPads, 58, 1, 3, mlt, lay, 0);
      std::vector<Channel> QL2P = retrieveChamber(stgcPads, 58, 2, 3, mlt, lay, 0);
      std::vector<Channel> QL3P = retrieveChamber(stgcPads, 58, 3, 3, mlt, lay, 0);
      std::vector<Channel> pad_channels;
      pad_channels.reserve(QL1P.size() + QL2P.size() + QL3P.size());
      pad_channels.insert(pad_channels.end(),
                          std::make_move_iterator(QL1P.begin()),
                          std::make_move_iterator(QL1P.end()));
      pad_channels.insert(pad_channels.end(),
                          std::make_move_iterator(QL2P.begin()),
                          std::make_move_iterator(QL2P.end()));
      pad_channels.insert(pad_channels.end(),
                          std::make_move_iterator(QL3P.begin()),
                          std::make_move_iterator(QL3P.end()));
      drawLayout(pad_channels);
    }
  }

  return 0;
}
