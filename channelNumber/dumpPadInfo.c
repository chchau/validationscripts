
struct Key {
  int m_sector{0};
  int m_stEta{0};
  int m_stPhi{0};
  int m_mlt{0};
  int m_layer{0};
  int m_chType{0};
  int m_ch{0};

  Key () {}
  Key(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch)
    : m_sector(stIndex), m_stEta(stEta), m_stPhi(stPhi), m_mlt(mlt), m_layer(gap), m_chType(chtype), m_ch(ch)
  {
  }

  int stationIndex() const {return m_sector;}
  int stationEta() const {return m_stEta;}
  int stationPhi() const {return m_stPhi;}
  int multiplet() const {return m_mlt;}
  int layer() const {return m_layer;}
  int channelType() const {return m_chType;}
  int channel() const {return m_ch;}

  bool isSameKey(const Key& k) {
    if (k.stationIndex() == m_sector && k.stationEta() == m_stEta && k.stationPhi() == m_stPhi &&
        k.multiplet() == m_mlt && k.layer() == m_layer && k.channelType() == m_chType && k.channel() == m_ch) {
      return true;
    }
    return false;
  }
};

struct point2D {
  double x{0.};
  double y{0.};
  
  point2D(): x(0.), y(0.) {}
  point2D(double a, double b): x(a), y(b) {}
};

struct Channel {
  Key m_key;
  int m_channel{0}, m_channelType{-1};
  double m_globalPosX{0.}, m_globalPosY{0.}, m_globalPosZ{0.};
  point2D m_corner1, m_corner2, m_corner3, m_corner4;
  Key m_clusterOT;
  Key m_clusterPrd;

  Channel(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch, 
          double gposX, double gposY, double gposZ) 
      : m_key(Key(stIndex, stEta, stPhi, mlt, gap, chtype, ch)),
        m_channelType(chtype),
        m_channel(ch),
        m_globalPosX(gposX),
        m_globalPosY(gposY),
        m_globalPosZ(gposZ)
  {
  }

  void insertCorners(point2D c1, point2D c2, point2D c3, point2D c4) {
    m_corner1 = c1;
    m_corner2 = c2;
    m_corner3 = c3;
    m_corner4 = c4;
  }

  void setClusterOnTrackID(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch) {
     m_clusterOT = Key(stIndex, stEta, stPhi, mlt, gap, chtype, ch);
  }

  void setClusterPrdID(int stIndex, int stEta, int stPhi, int mlt, int gap, int chtype, int ch) {
    m_clusterPrd = Key(stIndex, stEta, stPhi, mlt, gap, chtype, ch);
  }
};

std::vector<Channel> retrieveChamber(std::vector<Channel>& stgcChannels, int stIndex, int stEta, int stPhi,
                                     int mlt, int gap, int chType) {
  std::vector<Channel> outChamber;
  for (Channel& ch: stgcChannels) {
    if (ch.m_key.stationIndex()==stIndex && ch.m_key.stationEta()==stEta && ch.m_key.stationPhi()==stPhi && 
        ch.m_key.multiplet()==mlt && ch.m_key.layer()==gap && ch.m_key.channelType()==chType) {
      bool duplicate{false};
      for (const Channel& c: outChamber) {
        if (ch.m_key.channel() == c.m_key.channel()) {
          duplicate = true;
          break;
        }
      }
      if (!duplicate) outChamber.push_back(ch);
    }
  }

  return std::move(outChamber);
}

int dumpPadInfo () {

  const double HQ1 = 0.0;
  const double HQ2 = 1320.0; // in mm
  const double HQ3 = 1200.0; // in mm

  // Plotting style
  gStyle->SetTitleYOffset(1.2);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);

  std::cout << "... macro starting" << std::endl;
  TFile* file;
  file = new TFile("stgc_431914_lb0795_sf014_c.root");
  TTree* tree;
  tree = (TTree*) file->Get("BasicTesterTree");
  TTreeReader reader;
  reader.SetTree(tree);

  TTreeReaderArray<unsigned short> stgc_channel      (reader, "stgc_channel");
  TTreeReaderArray<vector<unsigned short>> stgcStripChannel      (reader, "stgcStripChannel");
  TTreeReaderArray<unsigned char> stgc_channel_type      (reader, "stgc_channel_type");
  TTreeReaderArray<unsigned char> stgc_gas_gap      (reader, "stgc_gas_gap");
  TTreeReaderArray<unsigned char> stgc_multiplet      (reader, "stgc_multiplet");
  TTreeReaderArray<unsigned char> stgc_stationPhi      (reader, "stgc_stationPhi");
  TTreeReaderArray<char> stgc_stationEta      (reader, "stgc_stationEta");
  TTreeReaderArray<unsigned char> stgc_stationIndex      (reader, "stgc_stationIndex");
  TTreeReaderArray<float> stgcGlobalPos_z      (reader, "stgcGlobalPos_z");
  TTreeReaderArray<float> stgcGlobalPos_x      (reader, "stgcGlobalPos_x");
  TTreeReaderArray<float> stgcGlobalPos_y      (reader, "stgcGlobalPos_y");
  TTreeReaderArray<float> stgcLocalPos_x      (reader, "stgcLocalPos_x");
  TTreeReaderArray<float> stgcLocalPos_y      (reader, "stgcLocalPos_y");
  //
  TTreeReaderArray<float> muons_pt      (reader, "muons_pt");
  TTreeReaderArray<float> muons_eta      (reader, "muons_eta");
  TTreeReaderArray<float> muons_phi      (reader, "muons_phi");
  TTreeReaderArray<float> muons_e      (reader, "muons_e");
  TTreeReaderArray<int> muons_q      (reader, "muons_q");
  //
  TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink      (reader, "stgcOnTrack_MuonLink");
  TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
  TTreeReaderArray<unsigned char> stgcOnTrack_channel_type      (reader, "stgcOnTrack_channel_type");
  TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap      (reader, "stgcOnTrack_gas_gap");
  TTreeReaderArray<unsigned char> stgcOnTrack_multiplet      (reader, "stgcOnTrack_multiplet");
  TTreeReaderArray<char> stgcOnTrack_stationEta      (reader, "stgcOnTrack_stationEta");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex      (reader, "stgcOnTrack_stationIndex");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi      (reader, "stgcOnTrack_stationPhi");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
  //
  TTreeReaderArray<float> stgcTrackStateGlobalPos_x      (reader, "stgcTrackStateGlobalPos_x");
  TTreeReaderArray<float> stgcTrackStateGlobalPos_y      (reader, "stgcTrackStateGlobalPos_y");
  TTreeReaderArray<float> stgcTrackStateGlobalPos_z      (reader, "stgcTrackStateGlobalPos_z");
  TTreeReaderArray<float> stgcTrackStateLocalPos_x      (reader, "stgcTrackStateLocalPos_x");
  TTreeReaderArray<float> stgcTrackStateLocalPos_y      (reader, "stgcTrackStateLocalPos_y");
  TTreeReaderArray<unsigned short> stgcTrackState_ClusterOnTrackLink      (reader, "stgcTrackState_ClusterOnTrackLink");
  TTreeReaderArray<unsigned short> stgcTrackState_MuonLink      (reader, "stgcTrackState_MuonLink");
  TTreeReaderArray<unsigned short> stgcTrackState_closestClusterLink      (reader, "stgcTrackState_closestClusterLink");
  TTreeReaderArray<float> stgcTrackState_closestClusterResidualX      (reader, "stgcTrackState_closestClusterResidualX");
  TTreeReaderArray<float> stgcTrackState_closestClusterResidualY      (reader, "stgcTrackState_closestClusterResidualY");
  TTreeReaderArray<unsigned short> stgcTrackState_channel      (reader, "stgcTrackState_channel");
  TTreeReaderArray<unsigned char> stgcTrackState_channel_type      (reader, "stgcTrackState_channel_type");
  TTreeReaderArray<unsigned char> stgcTrackState_gas_gap      (reader, "stgcTrackState_gas_gap");
  TTreeReaderArray<unsigned char> stgcTrackState_multiplet      (reader, "stgcTrackState_multiplet");
  TTreeReaderArray<unsigned short> stgcTrackState_padEta      (reader, "stgcTrackState_padEta");
  TTreeReaderArray<unsigned short> stgcTrackState_padPhi      (reader, "stgcTrackState_padPhi");
  TTreeReaderArray<char> stgcTrackState_stationEta      (reader, "stgcTrackState_stationEta");
  TTreeReaderArray<unsigned char> stgcTrackState_stationPhi      (reader, "stgcTrackState_stationPhi");
  TTreeReaderArray<unsigned char> stgcTrackState_stationIndex      (reader, "stgcTrackState_stationIndex");

  std::cout  << "...starting loop" << std::endl;
  int count{0}, nPads{0};
  std::vector<Channel> stgcPads;
  while (reader.Next()) {
    //if ((count/100) == 0) std::cout << "Entries " << count << std::endl;
    if (count > 1000) break;
    ++count;

    //std::cout  << "...debug_1" << std::endl;
    std::map<int, std::vector<Channel>> padTowers;

    int nHit = stgcTrackState_channel.GetSize(); //stgcOnTrack_channel.GetSize();
    for (int i = 0; i < nHit; ++i) {
      int stationIndex = static_cast<int>(stgcTrackState_stationIndex[i]); //stgcOnTrack_stationIndex[i]);
      int stationEta = static_cast<int>(stgcTrackState_stationEta[i]);     //stgcOnTrack_stationEta[i]);
      int stationPhi = static_cast<int>(stgcTrackState_stationPhi[i]);     //stgcOnTrack_stationPhi[i]);
      int multiplet = static_cast<int>(stgcTrackState_multiplet[i]);       //stgcOnTrack_multiplet[i]);
      int layer = static_cast<int>(stgcTrackState_gas_gap[i]);             //stgcOnTrack_gas_gap[i]);
      int channelType = static_cast<int>(stgcTrackState_channel_type[i]);  //stgcOnTrack_channel_type[i]);
      int chNumber = static_cast<int>(stgcTrackState_channel[i]);          //stgcOnTrack_channel[i]);

      int muIndex = static_cast<int>(stgcTrackState_MuonLink[i]); //stgcOnTrack_MuonLink[i]);
      float muPt = muons_pt[muIndex];
      //float muEta = muons_eta[muIndex];
      //float muPhi = muons_phi[muIndex];

      //std::cout  << "...debug_1-1" << std::endl;
      // Skip non pad hits
      if (channelType != 0) continue;
      if (muPt < 1.0) continue;

      Channel channel(stationIndex, stationEta, stationPhi, multiplet, layer, channelType, chNumber,
                      stgcTrackStateGlobalPos_x[i], stgcTrackStateGlobalPos_y[i], stgcTrackStateGlobalPos_z[i]);

      //std::cout  << "...debug_1-2" << std::endl;
      int iOT = stgcTrackState_ClusterOnTrackLink[i];
      if (iOT >= 0 && iOT < stgcOnTrack_stationIndex.GetSize()) {
        channel.setClusterOnTrackID(stgcOnTrack_stationIndex[iOT],
                                    stgcOnTrack_stationEta[iOT],
                                    stgcOnTrack_stationPhi[iOT],
                                    stgcOnTrack_multiplet[iOT],
                                    stgcOnTrack_gas_gap[iOT],
                                    stgcOnTrack_channel_type[iOT],
                                    stgcOnTrack_channel[iOT]);
      }
      //std::cout  << "...debug_1-3" << std::endl;

      int iPRD = stgcTrackState_closestClusterLink[i];
      if (iPRD >= 0 && iPRD < stgc_stationIndex.GetSize()) {
        channel.setClusterPrdID(stgc_stationIndex[iPRD],
                                stgc_stationEta[iPRD],
                                stgc_stationPhi[iPRD],
                                stgc_multiplet[iPRD],
                                stgc_gas_gap[iPRD],
                                stgc_channel_type[iPRD],
                                stgc_channel[iPRD]);
      }
      //std::cout  << "...debug_1-4" << std::endl;

      padTowers[muIndex].push_back(channel);
    }

    //std::cout  << "...debug_2" << std::endl;
    nPads = 0;
    std::vector<int> pad_stIndex, pad_stEta, pad_stPhi;
    for (const std::pair<int, std::vector<Channel>>& t: padTowers) {
      int mu_index = t.first;
      std::vector<Channel> pad_chnls = t.second;

      float muPt = muons_pt[mu_index];
      float muEta = muons_eta[mu_index];
      float muPhi = muons_phi[mu_index];

      std::cout << "muIdx: " << mu_index << " mupt: " << muPt << " mueta: " << muEta << " muphi: " << muPhi << std::endl;
      //std::cout << " ID: ";
      for (const auto& p: pad_chnls) {
        std::cout << "    ID: " << p.m_key.stationIndex() << ":" << p.m_key.stationEta() 
                  << ":" << p.m_key.stationPhi() << ":" << p.m_key.multiplet() << ":" << p.m_key.layer()
                  << ":" << p.m_key.channelType() << ":" << p.m_key.channel()
                  << ":(" << p.m_globalPosX << "," << p.m_globalPosY << "," << p.m_globalPosZ << ")"
                  << " OT: " << p.m_clusterOT.stationIndex() << ":" << p.m_clusterOT.stationEta() 
                  << ":" << p.m_clusterOT.stationPhi() << ":" << p.m_clusterOT.multiplet() 
                  << ":" << p.m_clusterOT.layer() << ":" << p.m_clusterOT.channelType() 
                  << ":" << p.m_clusterOT.channel()
                  << " Prd: " << p.m_clusterPrd.stationIndex() << ":" << p.m_clusterPrd.stationEta() 
                  << ":" << p.m_clusterPrd.stationPhi() << ":" << p.m_clusterPrd.multiplet() 
                  << ":" << p.m_clusterPrd.layer() << ":" << p.m_clusterPrd.channelType() 
                  << ":" << p.m_clusterPrd.channel();
        std::cout << std::endl;
        pad_stIndex.push_back(p.m_key.stationIndex());
        pad_stEta.push_back(p.m_key.stationEta());
        pad_stPhi.push_back(p.m_key.stationPhi());
      }
      std::cout << std::endl;
      ++nPads;
    }
    //std::cout  << "...debug_3" << std::endl;

    //std::cout  << "...debug_3" << std::endl;
    if (nPads > 0) {
      int nPrd = stgc_channel.GetSize();
      for (int i = 0; i < nPrd; ++i) {
        if (static_cast<int>(stgc_channel_type[i]) == 0){
          int prdIndex = static_cast<int>(stgc_stationIndex[i]);
          int prdEta = static_cast<int>(stgc_stationEta[i]);
          int prdPhi = static_cast<int>(stgc_stationPhi[i]);
          int prdMlt = static_cast<int>(stgc_multiplet[i]);
          int prdLayer = static_cast<int>(stgc_gas_gap[i]);
          int prdChannel = static_cast<int>(stgc_channel[i]);

          bool foundPrd{false};
          for (size_t j = 0; j < pad_stIndex.size(); ++j) {
            if (prdIndex == pad_stIndex[j] && prdEta == pad_stEta[j] && prdPhi == pad_stPhi[j])
              foundPrd = true;
          }
          if (foundPrd) {
            std::cout << "PRD: " << count
                      << " " << prdIndex << " " << prdEta << " " << prdPhi << " " 
                      << prdMlt << " " << prdLayer << " " << prdChannel
                      << " " << stgcGlobalPos_x[i] << "," << stgcGlobalPos_y[i] << "," << stgcGlobalPos_z[i]
                      << std::endl;
          }
        }
      }
    }
  }

  std::cout  << "...Closing macro..." << std::endl;
  return 0;
}
