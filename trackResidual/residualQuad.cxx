/// Compute the residual per quad
/// 1. Get the hits on track associated to each muon
/// 2. Process each muon track and find the layer closest to the IP
/// 3. Calculate the residuals

// Channel type: Pad = 0, Strip = 1, Wire = 2
enum CHTYPE {PAD=0, STRIP, WIRE};

const int NLAYER = 8;
const int NSECTOR = 16;
const int NSTATIONETA = 6;
const int NMLT = 2;

struct Hit {
  int m_stationIndex{0};
  int m_stationEta{0};
  int m_stationPhi{0};
  int m_multiplet{0};
  int m_layer{0};
  int m_channelType{0};
  int m_channel{0};
  int m_muonIndex{-1};
  float m_residual{-9.9};
  float m_localPos_x{0.};
  float m_localPos_y{0.};
  float m_globalPos_x{0.};
  float m_globalPos_y{0.};
  float m_globalPos_z{0.};

  Hit() {}

  Hit(int stIndex, int stEta, int stPhi, int mlt, int layer, int chType, int channel, int muIndex,
      float residual, float localposX, float localposY, float globalposX, float globalposY, float globalposZ)
          : m_stationIndex(stIndex), m_stationEta(stEta), m_stationPhi(stPhi),
            m_multiplet(mlt), m_layer(layer), m_channelType(chType),
            m_channel(channel), m_muonIndex(muIndex), m_residual(residual),
            m_localPos_x(localposX), m_localPos_y(localposY),
            m_globalPos_x(globalposX), m_globalPos_y(globalposY), m_globalPos_z(globalposZ)
  {
  }
};

struct Track {
  int m_id{-1};
  int m_muonIndex{-1};
  // Channel type: Pad = 0, Strip = 1, Wire = 2
  std::array<std::vector<Hit>, 3> m_hits;

  Track() {}
  Track(int trkId, int muId) : m_id(trkId), m_muonIndex(muId) {}

  void insertHit(int channelType, Hit hit) {
    m_hits[channelType].push_back(hit);
  }
};

struct TagHisto {
  int m_sector{0};
  int m_stationEta{0};
  int m_multiplet{0};
  int m_gasgap{0};
  TH1F* m_h1f{nullptr};
  std::vector<float> m_fitParam;
  std::array<TH1F*, 4> m_histLayer{nullptr, nullptr, nullptr, nullptr};
  std::vector<std::vector<float>> m_fitParamLayer{ {0.}, {0.}, {0.}, {0.} };

  TagHisto() {}

  TagHisto(int sec, int stEta, int mlt, int gasgap)
    : m_sector(sec),
      m_stationEta(stEta),
      m_multiplet(mlt),
      m_gasgap(gasgap)
  {
  }

  ~TagHisto() {
    if (m_h1f) {delete m_h1f; m_h1f = nullptr;}

    if (!m_histLayer.empty()) {
      for (TH1F* h: m_histLayer) {
        if (h) {delete h; h = nullptr;}
      }
    }
  }

  bool isSameHisto(int sec, int stEta, int mlt, int gap) {
    return (sec == m_sector &&
            stEta == m_stationEta &&
            mlt == m_multiplet &&
            gap == m_gasgap);
  }

  void setIdentifier(int sec, int stEta, int mlt, int gap) {
    m_sector = sec;
    m_stationEta = stEta;
    m_multiplet = mlt;
    m_gasgap = gap;
  }

  bool addIdentifier(int sec, int stEta, int mlt, int gap) {
    if (m_sector == 0 || m_stationEta == 0 || m_multiplet == 0) {
      setIdentifier(sec, stEta, mlt, gap);
      return true;
    } else {
      return isSameHisto(sec, stEta, mlt, gap);
    }
  }

  void setFitParameters(std::vector<float>& par) {
    m_fitParam = par;
  }
  std::vector<float> getFitParameters() const {
    return m_fitParam;
  }
};

// Helpers functions
namespace Utils {
  int findMuonTrack(const std::vector<Track>& muonTracks, int muIndex) {
    for (size_t i = 0; i < muonTracks.size(); ++i) {
      if (muIndex == muonTracks.at(i).m_muonIndex) {
        return static_cast<int>(i);
      }
    }
    return -1;
  }
  
  int computeKey(int stIndex, int stEta, int stPhi, int multiplet, int gasgap) {
      int key = stIndex * 1000 + std::abs(stEta) * 100 + stPhi * 10  + (multiplet - 1) * 4 + gasgap;
      if (stEta < 0) key *= -1;
      return key;
  }
  
  int sector(int stationIndex, int stationPhi) {
    return (stationPhi * 2 - (stationIndex - 57));
  }
  
  int quadIndex(int sector, int stationEta, int multiplet) {
    int linearEta = (stationEta > 0)? stationEta : std::abs(stationEta)+3;
    return (sector - 1) * NSTATIONETA * NMLT + (linearEta - 1) * NMLT + (multiplet - 1);
  }
  
  // Coompute the sector, stationEta and multiplet indices from the quad index
  std::array<int, 3> decodeQuadIndex(int index) {
    std::array<int, 3> id {0, 0, 0};
    // Sector
    id[0] = std::floor(index / (NSTATIONETA * NMLT)) + 1;
    // stationEta
    int sEta = std::floor((index - (id[0] - 1) * NSTATIONETA * NMLT) / NMLT);
    id[1] = (sEta < 3)? sEta + 1 : -1 * (sEta - 2);
    // multiplet
    id[2] = index - (id[0] - 1) * NSTATIONETA * NMLT - (id[1] - 1) * NMLT + 1;
  
    return id;
  }
}


// Main macro
int residualQuad ()
{
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  //gStyle->SetOptStat(0);
  gStyle->SetOptFit();

  bool enableCustomizedResidual = false;
  bool enableFittingPerLayer = true;

  TString outputDir = "plots/";
  //TString inDatasetPath = "/eos/atlas/atlasgroupdisk/det-muon/dq2/rucio/user/dpizzi/";
  //TString datasetListName = "/eos/user/c/chchau/sw/devArea/digitStudy/residual/ds_440613asBuilt.txt";
  TString inDatasetPath = "/eos/atlas/atlasgroupdisk/det-muon/dq2/rucio/group/det-muon/";
  TString datasetListName = "ds_440613asBuiltOnlyTranslation.txt";
  //TString inDatasetPath = "/eos/atlas/atlastier0/rucio/data23_13p6TeV/physics_Main/00454222/data23_13p6TeV.00454222.physics_Main.merge.NTUP_MUTEST.f1360_m2180_c1404_m2100/";
  //TString datasetListName = "ds_454222.txt";
  std::ifstream inDatasetFile;
  inDatasetFile.open(datasetListName);
  std::vector<TString> datasetList;

  if (inDatasetFile.is_open()) {
    std::string line;
    while (std::getline(inDatasetFile, line)) {
      if (line[0] == '#') continue;
      datasetList.push_back(line.c_str());
    }
    inDatasetFile.close(); // CLose input file
  }
  std::cout << "...Reading the following root files" << std::endl;
  for (auto f: datasetList) {
    std::cout << f << std::endl;
  }

  TChain* fChain = new TChain("BasicTesterTree");
  //std::cout << "...Processing the following root files" << std::endl;
  for (auto ds: datasetList) {
    TString filename = inDatasetPath + ds;
    fChain->Add(filename);
    //std::cout << filename << std::endl;
  }
  TTreeReader reader;
  reader.SetTree(fChain);

  TTreeReaderArray<float> muons_pt   (reader, "muons_pt");
  TTreeReaderArray<float> muons_eta  (reader, "muons_eta");
  TTreeReaderArray<float> muons_phi  (reader, "muons_phi");
  TTreeReaderArray<float> muons_e    (reader, "muons_e");
  TTreeReaderArray<int> muons_q      (reader, "muons_q");
  TTreeReaderArray<unsigned short> muons_author  (reader, "muons_author");
  //
  TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink      (reader, "stgcOnTrack_MuonLink");
  TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
  TTreeReaderArray<unsigned char> stgcOnTrack_channel_type      (reader, "stgcOnTrack_channel_type");
  TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap      (reader, "stgcOnTrack_gas_gap");
  TTreeReaderArray<unsigned char> stgcOnTrack_multiplet      (reader, "stgcOnTrack_multiplet");
  TTreeReaderArray<char> stgcOnTrack_stationEta      (reader, "stgcOnTrack_stationEta");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex      (reader, "stgcOnTrack_stationIndex");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi      (reader, "stgcOnTrack_stationPhi");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
  TTreeReaderArray<int>   stgcOnTrackNStrips       (reader, "stgcOnTrackNStrips");
  TTreeReaderArray<float> stgcOnTrackPullTrack     (reader, "stgcOnTrackPullTrack");
  TTreeReaderArray<float> stgcOnTrackPullTruth     (reader, "stgcOnTrackPullTruth");
  TTreeReaderArray<float> stgcOnTrackResidualTrack (reader, "stgcOnTrackResidualTrack");
  TTreeReaderArray<float> stgcOnTrackResidualTruth (reader, "stgcOnTrackResidualTruth");
  TTreeReaderArray<float> stgcOnTrackLocalPos_x     (reader, "stgcOnTrackLocalPos_x");
  TTreeReaderArray<float> stgcOnTrackLocalPos_y     (reader, "stgcOnTrackLocalPos_y");
  TTreeReaderArray<float> stgcOnTrackError         (reader, "stgcOnTrackError");
  TTreeReaderArray<vector<int>> stgcOnTrackStripCharges = {reader, "stgcOnTrackStripCharges"};
  TTreeReaderArray<vector<short>> stgcOnTrackStripDriftTimes = {reader, "stgcOnTrackStripDriftTimes"};
  TTreeReaderArray<vector<unsigned short>> stgcOnTrackStripNumbers = {reader, "stgcOnTrackStripNumbers"};

  // Map for making histograms of overall residuals
  std::map<std::string, int> histoName{{"rPad", 0}, {"rStrip", 1}, {"rWire", 2}};
  std::vector<TH1F*> v_histos;
  for (const auto& [key, value]: histoName) {
    TString hname{key};
    v_histos.push_back(new TH1F(hname, "", 200, -5., 5.));
  }

  // Residual histograms for each quad
  const int nQuad = NSECTOR * NSTATIONETA * NMLT;
  std::array<TagHisto, nQuad> hv_residualQuad;
  for (int i = 0; i < nQuad; ++i) {
    TagHisto& tagH = hv_residualQuad.at(i);
    // Initialize the per-quad histograms
    TString histoName = TString::Format("residualQuad%d", i);
    tagH.m_h1f = new TH1F(histoName, "", 100, -2.5, 2.5);
  }

  // Standard track residual per quad
  std::array<TagHisto, nQuad> hv_tkResidualQuad;
  for (int i = 0; i < nQuad; ++i) {
    TagHisto& tagH = hv_tkResidualQuad.at(i);
    // Initialize the per-quad histograms
    TString histoName = TString::Format("otResidualQuad%d", i);
    tagH.m_h1f = new TH1F(histoName, "", 100, -2.5, 2.5);

    // Initialize the per-layer histos
    for (int g = 0; g < NLAYER/2; ++g) {
      histoName = TString::Format("otResQuad%d-L%d", i, g+1);
      tagH.m_histLayer.at(g) = new TH1F(histoName, "", 100, -2.5, 2.5);
    }
  }

  int nMultipleHit = 0;
  int nDiffQuad = 0;
  int nTracks = 0;

  std::cout  << "...starting loop" << std::endl;
  int nEvent{0};
  while (reader.Next()) {
    //if (nEvent > 100) break;
    //std::cout  << "event: " << nEvent << std::endl;

    // muon tracks in a event
    std::vector<Track> muonTracks;

    int nMuon = muons_pt.GetSize();
    Track muTrk;
    for (int i = 0; i < nMuon; ++i) {
      muTrk.m_id = i;
      muTrk.m_muonIndex = i;
      muonTracks.push_back(muTrk);

      int muPt = muons_pt[i];
      int muAuthor = muons_author[i];
      if (muPt < 15. || (muAuthor != 1 && muAuthor != 5)) continue;

      ++nTracks;
    }


    // Find the indices of OnTrack hits associated to the muon track
    int nOT = stgcOnTrack_channel.GetSize();
    std::vector<int> badOTIndices;
    for (int i = 0; i < nOT; ++i) {
      int muIndex = static_cast<int>(stgcOnTrack_MuonLink[i]);
      int stIndex = static_cast<int>(stgcOnTrack_stationIndex[i]);
      int stEta = static_cast<int>(stgcOnTrack_stationEta[i]);
      int stPhi = static_cast<int>(stgcOnTrack_stationPhi[i]);
      int mlt = static_cast<int>(stgcOnTrack_multiplet[i]);
      int gap = static_cast<int>(stgcOnTrack_gas_gap[i]);
      int channelType = static_cast<int>(stgcOnTrack_channel_type[i]);
      int channel = static_cast<int>(stgcOnTrack_channel[i]);
      float residual = stgcOnTrackResidualTrack[i];
      float locPos_x = stgcOnTrackLocalPos_x[i];
      float locPos_y = stgcOnTrackLocalPos_y[i];
      float globPos_x = stgcOnTrackGlobalPos_x[i];
      float globPos_y = stgcOnTrackGlobalPos_y[i];
      float globPos_z = stgcOnTrackGlobalPos_z[i];
      // Create a hit
      Hit otHit(stIndex, stEta, stPhi, mlt, gap, channelType, channel, muIndex, residual,
                locPos_x, locPos_y, globPos_x, globPos_y, globPos_z);

      int trkIndex = Utils::findMuonTrack(muonTracks, muIndex);
      if (trkIndex >= 0) {
        muonTracks.at(trkIndex).insertHit(channelType, otHit);
      } else {
        badOTIndices.push_back(i);
      }

      if (channelType == STRIP) {
        int muPt = muons_pt[muIndex];
        int muAuthor = muons_author[muIndex];
        if (muPt < 15. || (muAuthor != 1 && muAuthor != 5)) continue;
        v_histos.at(histoName["rStrip"])->Fill(residual);

        // Fill histogram
        int otSector = Utils::sector(stIndex, stPhi);
        int histo_ind = Utils::quadIndex(otSector, stEta, mlt);

        if (hv_tkResidualQuad[histo_ind].m_h1f) {
          if (!hv_tkResidualQuad[histo_ind].addIdentifier(otSector, stEta, mlt, 0)) {
            std::cout << "Error  Not same OT residual histo" << std::endl;
            continue;
          }
          // per-quad
          hv_tkResidualQuad[histo_ind].m_h1f->Fill(residual);

          // per-layer
          hv_tkResidualQuad[histo_ind].m_histLayer[gap-1]->Fill(residual);
        }
      }
    }

    if (!badOTIndices.empty()) {
      std::cout << "badOTIndices: ";
      for (auto v: badOTIndices)
        std::cout << "{" << v << "} ";
      std::cout << std::endl;
    }

    if (enableCustomizedResidual) {

      // Get the number of hits per track
      int nMultipleHit = 0;
      int nDiffQuad = 0;
      for (const Track& t: muonTracks) {

        int muPt = muons_pt[t.m_muonIndex];
        int muAuthor = muons_author[t.m_muonIndex];
        if (muPt < 15. || (muAuthor != 1 && muAuthor != 5)) continue;

        //++nTracks;

        // Sort the hits/clusters assigned to the track
        std::array<Hit, NLAYER> sortedHits;
        for (Hit h: t.m_hits[STRIP]) {

          int iLayer = (h.m_multiplet - 1) * 4 +  (h.m_layer - 1);
          if (sortedHits[iLayer].m_layer > 0) {

            // Check if the hit/cluster is in the same quad
            if (h.m_stationIndex == sortedHits[iLayer].m_stationIndex &&
                h.m_stationEta == sortedHits[iLayer].m_stationEta &&
                h.m_stationPhi == sortedHits[iLayer].m_stationPhi &&
                h.m_multiplet == sortedHits[iLayer].m_multiplet)
            {
              ++nMultipleHit;
              // If hit/cluster on the same surface, then pick the one closest to the muon track
              if (h.m_residual < sortedHits[iLayer].m_residual) sortedHits[iLayer] = h;
              continue;
            } else {
              ++nDiffQuad;
              //std::cout << "Expecting cluster on"
              //          << " stIndex: " << static_cast<int>(stgcOnTrack_stationIndex[sortedOTIndices[ind_ref]])
              //          << " stEta: " << static_cast<int>(stgcOnTrack_stationEta[sortedOTIndices[ind_ref]])
              //          << " stPhi: " << static_cast<int>(stgcOnTrack_stationPhi[sortedOTIndices[ind_ref]])
              //          << " mlt: " << static_cast<int>(stgcOnTrack_multiplet[sortedOTIndices[ind_ref]])
              //          << " gap: " << static_cast<int>(stgcOnTrack_gas_gap[sortedOTIndices[ind_ref]])
              //          << ", but found stIndex: " << stIndex << " stEta: " << stEta << " stPhi: " << stPhi
              //          << " mlt: " << mlt << " gap: " << gap
              //          << std::endl;
              continue;
            }
          } else {
            sortedHits[iLayer] = h;
          }
        }

        std::array<int, 4> quad1_id = {0};
        std::array<int, 4> quad2_id = {0};
        bool foundDiffQuad{false};
        for (Hit ihit: sortedHits) {
          if (ihit.m_multiplet == 0) continue;

          std::array<int, 4>& quad_id = (ihit.m_multiplet == 1)? quad1_id : quad2_id;
          if (quad_id[0] == 0) {
            quad_id[0] = ihit.m_stationIndex;
            quad_id[1] = ihit.m_stationEta;
            quad_id[2] = ihit.m_stationPhi;
            quad_id[3] = ihit.m_multiplet;
            continue;
          } else {
            if (ihit.m_stationIndex != quad_id[0] ||
                ihit.m_stationEta != quad_id[1] ||
                ihit.m_stationPhi != quad_id[2] ||
                ihit.m_multiplet != quad_id[3]) {
              ++nDiffQuad;
              foundDiffQuad = true;
              break;
            }
          }
        }
        if (foundDiffQuad) continue;
            
        // Compute the residual between layers
        int ind_quad1_layer1{-1};
        int ind_quad2_layer1{-1};
        for (int j = 0; j < NLAYER; ++j) {
          if (sortedHits[j].m_layer == 0) continue;

          int& ind_layer1 = (j < NLAYER/2)? ind_quad1_layer1 : ind_quad2_layer1;
          if (ind_layer1 < 0) {
            ind_layer1 = j;
            continue;
          }

          if (sortedHits[j].m_multiplet != sortedHits[ind_layer1].m_multiplet) {
            std::cout << "Error  expected multiplet " << sortedHits[ind_layer1].m_multiplet 
                      << ", but got " << sortedHits[j].m_multiplet << std::endl;
            continue;
          }

          double deltaR = sortedHits[j].m_localPos_x - sortedHits[ind_layer1].m_localPos_x;
          double deltaZ = TMath::Abs(sortedHits[j].m_globalPos_z - sortedHits[ind_layer1].m_globalPos_z);
          float muEta = muons_eta[t.m_muonIndex];
          float muTheta = 2 * TMath::ATan(TMath::Exp(-muEta));
          if (muTheta > TMath::Pi()/2) muTheta = TMath::Pi() - muTheta;
          float newRes = deltaR - deltaZ * TMath::Tan(muTheta);
          
          // Fill histos
          int stIndex = sortedHits[j].m_stationIndex;
          int stEta = sortedHits[j].m_stationEta;
          int stPhi = sortedHits[j].m_stationPhi;
          int mlt = sortedHits[j].m_multiplet;
          int newSector = Utils::sector(stIndex, stPhi);
          int histo_ind = Utils::quadIndex(newSector, stEta, mlt);
          if (hv_residualQuad[histo_ind].m_h1f) {
            if (!hv_residualQuad[histo_ind].addIdentifier(newSector, stEta, mlt, 0)) {
              std::cout << "Error  Not same histo" << std::endl;
              continue;
            }
            hv_residualQuad[histo_ind].m_h1f->Fill(newRes);
          }
        }
      } //End: for-loop over muon tracks
    } // End: enable customized residual calculation

    ++nEvent;
  }
  std::cout  << "...Processed " << nEvent << " events" << std::endl;
  std::cout  << "nTracks: " << nTracks << ", Multiple hits: " << nMultipleHit << ", diffQuad: " << nDiffQuad << std::endl;
  
  TCanvas c1 ("c1", "canvas", 800, 600);
  
  for (const auto& [key, value]: histoName) {
    if (value != STRIP) continue;
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.10);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
  
    v_histos.at(value)->GetXaxis()->SetTitle("Residual [mm]");
    v_histos.at(value)->GetYaxis()->SetTitle("Entries");
    float hmean = v_histos.at(value)->GetMean();
    float hrms = v_histos.at(value)->GetRMS();
  
    TF1 *g1 = new TF1("g1", "gaus", hmean-1.5*hrms, hmean+1.5*hrms);
    TF1 *g2 = new TF1("g2", "gaus", -5.0, 5.0);
    TF1 *total = new TF1("total", "gaus(x,[0],[1],[2])+gaus(x,[3],[1],[4])", -5.0, 5.0);
    //g1->SetLineColor(0);
    //g2->SetLineColor(0);
    total->SetLineColor(2);
    // Fit options: "R" to limit the fit to the range, "+" to avoid replacing the previous fit 
    v_histos.at(value)->Fit(g1, "R");
    v_histos.at(value)->Fit(g2, "R+");
    double fitpar[6];
    g1->GetParameters(&fitpar[0]);
    g2->GetParameters(&fitpar[3]);
    total->SetParameters(fitpar[0],fitpar[1],fitpar[2],fitpar[3],fitpar[5]);
    v_histos.at(value)->Fit(total, "R");
  
    TString hname = "residuals_" + key;
    c1.Print(outputDir + hname + ".png");
  
    delete g1; delete g2; delete total;
  }
  
  TH2F* h_otResQuadSigma = new TH2F("otResquadsig", "", 17, 0., 17, 12, -6., 6.);
  TH2F* h_otResQuadMean = new TH2F("otResquadmean", "", 17, 0., 17, 12, -6., 6.);
  // plot track residual hitos per quad
  for (TagHisto& hist: hv_tkResidualQuad) {
    if (!hist.m_h1f) continue;
    TString histFilename = TString::Format("ot_residualPerQuad_sector%d_stEta%d_mlt%d",
                                           hist.m_sector, hist.m_stationEta, hist.m_multiplet);
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.12);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
    TString ylabel = TString::Format("Entries of sector: %d stationEta: %d multiplet: %d",
                     hist.m_sector, hist.m_stationEta, hist.m_multiplet);
    hist.m_h1f->GetYaxis()->SetTitle(ylabel);
    hist.m_h1f->GetXaxis()->SetTitle("Track residual per quad [mm]");
  
    float hmean = hist.m_h1f->GetMean();
    float hrms = hist.m_h1f->GetRMS();
    TF1 *g1 = new TF1("g1", "gaus", hmean-1.5*hrms, hmean+1.5*hrms);
    TF1 *g2 = new TF1("g2", "gaus", -2.5, 2.5);
    TF1 *total = new TF1("total", "gaus(0)+gaus(3)", -2.5, 2.5);
    total->SetLineColor(2);
    hist.m_h1f->Fit(g1, "R");
    hist.m_h1f->Fit(g2, "R+");
    double fitpar[6];
    g1->GetParameters(&fitpar[0]);
    g2->GetParameters(&fitpar[3]);
    total->SetParameters(fitpar);
    hist.m_h1f->Fit(total, "R");
  
    c1.Print(outputDir + histFilename + ".png");
  
    total->GetParameters(&fitpar[0]);
  
    // 2D histos
    float xpos = hist.m_sector;
    float ypos = ((std::abs(hist.m_stationEta) - 1) * NMLT + (hist.m_multiplet - 1));
    if (hist.m_stationEta < 0)
      ypos = -1 * ((std::abs(hist.m_stationEta) - 1) * NMLT + (hist.m_multiplet - 1)) - 1;
    // Find the correct standrd deviation
    double stdev = fitpar[2];
    double mean = fitpar[1];
    if (fitpar[5] > 0 && fitpar[2] > fitpar[5]) {
      stdev = fitpar[5];
      mean = fitpar[4];
    }
    float res_sig = static_cast<int>(stdev >= 0 ? stdev * 1000 + 0.5 : stdev * 1000 - 0.5) / 1000.0;
    float res_mu  = static_cast<int>(mean >= 0 ? mean * 1000 + 0.5 : mean * 1000 - 0.5) / 1000.0;
    h_otResQuadSigma->Fill(xpos, ypos, res_sig);
    h_otResQuadMean->Fill(xpos, ypos, res_mu);
  
    // Plot the residual per layer
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.12);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
    //ylabel = TString::Format("Entries of sector: %d stationEta: %d multiplet: %d layer: %d",
    //                 hist.m_sector, hist.m_stationEta, hist.m_multiplet, g);
    TH1F* h_dummy = (TH1F*)hist.m_histLayer.at(0)->Clone("dummy");
    h_dummy->SetStats(0);
    h_dummy->GetXaxis()->SetTitle("Residual per layer [mm]");
    h_dummy->GetYaxis()->SetTitle(ylabel);
    // Find maximum
    float hist_max = hist.m_histLayer[0]->GetMaximum();
    for (int g = 1; g < 4; ++g) {
      float tmp_max = hist.m_histLayer[g]->GetMaximum();
      if (tmp_max > hist_max) hist_max = tmp_max;
    }
    hist_max = std::ceil((1.1 * hist_max) / 10) * 10.0;
    h_dummy->SetMaximum(hist_max);
    h_dummy->SetLineColor(0);
    h_dummy->SetMarkerColor(0);
    h_dummy->Draw();
    //int linecolor[4] = {922, 597, 633, 417};
    int linecolor[4] = {1, 633, 601, 417};
    // Overlay histograms
    for (int g = 0; g < 4; ++g) {
      if (hist.m_histLayer[g]->GetEntries() < 100) continue;
      hist.m_histLayer[g]->SetLineColor(linecolor[g]);
      // Fit
      if (enableFittingPerLayer) {
        float tmpMean = hist.m_histLayer[g]->GetMean();
        float tmpRms = hist.m_histLayer[g]->GetRMS();
        TF1* gc1 = new TF1("gc1", "gaus", tmpMean-1.5*tmpRms, tmpMean+1.5*tmpRms);
        TF1* gc2 = new TF1("gc2", "gaus", -2.5, 2.5);
        TF1* gcTotal = new TF1("gcTotal", "gaus(0)+gaus(3)", -2.5, 2.5);
        hist.m_histLayer[g]->Fit(gc1, "R0");
        hist.m_histLayer[g]->Fit(gc2, "R0");
        double tmpPar[6];
        gc1->GetParameters(&tmpPar[0]);
        gc2->GetParameters(&tmpPar[3]);
        gcTotal->SetParameters(tmpPar);
        hist.m_histLayer[g]->Fit(gcTotal, "R0");
        gcTotal->GetParameters(&tmpPar[0]);
        hist.m_fitParamLayer[g] = std::vector<float>(tmpPar, tmpPar+6);
        //
        delete gc1; delete gc2; delete gcTotal;
      }
      hist.m_histLayer[g]->SetLineWidth(2);
      hist.m_histLayer[g]->Draw("same hist");
    }
    c1.Print(outputDir + "lay_" + histFilename + ".png");
  
    delete g1; delete g2; delete total;
  }
  
  TCanvas c2 ("c2", "canvas", 840, 600);
  //c2.Clear();
  c2.SetRightMargin(0.10);
  c2.SetLeftMargin(0.12);
  c2.SetTopMargin(0.02);
  c2.SetBottomMargin(0.08);
  h_otResQuadSigma->SetStats(0);
  h_otResQuadSigma->GetYaxis()->SetTitle("(StationEta - 1) * 2 + (Multiplet - 1)");
  h_otResQuadSigma->GetXaxis()->SetTitle("Sector");
  h_otResQuadSigma->Draw("text colz");
  c2.Print(outputDir + "2D_ot_residualMap_sigma.png");
  c2.Print(outputDir + "2D_ot_residualMap_sigma.svg");
  c2.Clear();
  c2.SetRightMargin(0.10);
  c2.SetLeftMargin(0.12);
  c2.SetTopMargin(0.02);
  c2.SetBottomMargin(0.08);
  h_otResQuadMean->SetStats(0);
  h_otResQuadMean->GetYaxis()->SetTitle("(StationEta - 1) * 2 + (Multiplet - 1)");
  h_otResQuadMean->GetXaxis()->SetTitle("Sector");
  h_otResQuadMean->Draw("text colz");
  c2.Print(outputDir + "2D_ot_residualMap_mean.png");
  c2.Print(outputDir + "2D_ot_residualMap_mean.svg");

  // Dump residual mean and sigma to a file
  if (enableFittingPerLayer) {
    std::ofstream resFile;
    resFile.open("layer_residual_info.dat", ios::out);
    for (TagHisto& hist: hv_tkResidualQuad) {
      if (!hist.m_h1f) continue;

      const auto default_precision {std::cout.precision()};
      TString chamber_id = TString::Format("%d, %d, %d", hist.m_sector,
                                                             hist.m_stationEta,
                                                             hist.m_multiplet);

      float refLayerMean{-1.0}, refLayerSigma{-1.0};
      int refLayerIndex = (hist.m_multiplet==1)? 3 : 0;

      if (hist.m_fitParamLayer.at(refLayerIndex).size() > 2) {
        refLayerMean = hist.m_fitParamLayer.at(refLayerIndex).at(1);
        refLayerSigma = hist.m_fitParamLayer.at(refLayerIndex).at(2);
      }

      resFile << std::setprecision(3);
      for (int i = 0; i < 4; ++i) {
        float tmpMean{99.9}, tmpSigma{-1.};
        if (hist.m_fitParamLayer.at(i).size() > 2) {
          tmpMean = hist.m_fitParamLayer.at(i).at(1);
          tmpSigma = hist.m_fitParamLayer.at(i).at(2);
        }
        if (tmpSigma > 0.) {
          resFile << chamber_id << ", " << i+1 << ", " << tmpMean << ", " << tmpSigma;
        } else {
          resFile << chamber_id << ", " << i+1 << ", , ";
        }

        if (refLayerSigma > 0. && tmpSigma > 0.) {
          resFile << ", " << (tmpMean - refLayerMean) << ", " << (tmpSigma - refLayerSigma);
        } else {
          resFile << ", , ";
        }
        resFile << std::endl;
      }
      resFile << std::setprecision(default_precision);
    }
    resFile.close();
  }

  if (enableCustomizedResidual) {
    TH2F* h_resQuadSigma = new TH2F("resquadsig", "", 17, 0., 17., 12, -6., 6.);
    TH2F* h_resQuadMean = new TH2F("resquadmean", "", 17, 0., 17., 12, -6., 6.);
    // plot user-defined residual hitos per quad
    for (TagHisto& hist: hv_residualQuad) {
      if (!hist.m_h1f) continue;
      TString histFilename = TString::Format("residualPerQuad_sector%d_stEta%d_mlt%d",
                                             hist.m_sector, hist.m_stationEta, hist.m_multiplet);
      c1.Clear();
      c1.SetRightMargin(0.02);
      c1.SetLeftMargin(0.12);
      c1.SetTopMargin(0.02);
      c1.SetBottomMargin(0.08);
      TString ylabel = TString::Format("Entries of sector: %d stationEta: %d multiplet: %d",
                       hist.m_sector, hist.m_stationEta, hist.m_multiplet);
      hist.m_h1f->GetYaxis()->SetTitle(ylabel);
      hist.m_h1f->GetXaxis()->SetTitle("Residual per quad [mm]");
      //hist.m_h1f->Draw("HIST");
  
      float hmean = hist.m_h1f->GetMean();
      float hrms = hist.m_h1f->GetRMS();
      TF1 *g1 = new TF1("g1", "gaus", hmean-1.5*hrms, hmean+1.5*hrms);
      TF1 *g2 = new TF1("g2", "gaus", -2.5, 2.5);
      TF1 *total = new TF1("total", "gaus(0)+gaus(3)", -2.5, 2.5);
      total->SetLineColor(2);
      hist.m_h1f->Fit(g1, "R");
      hist.m_h1f->Fit(g2, "R+");
      double fitpar[6];
      g1->GetParameters(&fitpar[0]);
      g2->GetParameters(&fitpar[3]);
      total->SetParameters(fitpar);
      hist.m_h1f->Fit(total, "R");
  
      c1.Print(outputDir + histFilename + ".png");
  
      total->GetParameters(&fitpar[0]);
  
      // 2D histos
      float xpos = hist.m_sector;
      float ypos = ((std::abs(hist.m_stationEta) - 1) * NMLT + (hist.m_multiplet - 1));
      if (hist.m_stationEta < 0)
        ypos = -1 * ((std::abs(hist.m_stationEta) - 1) * NMLT + (hist.m_multiplet - 1)) - 1;
      // Find the correct standrd deviation
      double stdev = fitpar[2];
      double mean = fitpar[1];
      if (fitpar[2] > fitpar[5]) {
        stdev = fitpar[5];
        mean = fitpar[4];
      }
      float res_sig = static_cast<int>(stdev >= 0 ? stdev * 1000 + 0.5 : stdev * 1000 - 0.5) / 1000.0;
      float res_mu  = static_cast<int>(mean >= 0 ? mean * 1000 + 0.5 : mean * 1000 - 0.5) / 1000.0;
      h_resQuadSigma->Fill(xpos, ypos, res_sig);
      h_resQuadMean->Fill(xpos, ypos, res_mu);
  
      delete g1; delete g2; delete total;
    }

    c2.Clear();
    c2.SetRightMargin(0.10);
    c2.SetLeftMargin(0.12);
    c2.SetTopMargin(0.02);
    c2.SetBottomMargin(0.08);
    h_resQuadSigma->SetStats(0);
    h_resQuadSigma->GetYaxis()->SetTitle("(StationEta - 1) * 2 + (Multiplet - 1)");
    h_resQuadSigma->GetXaxis()->SetTitle("Sector");
    h_resQuadSigma->Draw("text colz");
    c2.Print(outputDir + "2D_residualMap_sigma.png");
    c2.Print(outputDir + "2D_residualMap_sigma.svg");
    c2.Clear();
    c2.SetRightMargin(0.10);
    c2.SetLeftMargin(0.12);
    c2.SetTopMargin(0.02);
    c2.SetBottomMargin(0.08);
    h_resQuadMean->SetStats(0);
    h_resQuadMean->GetYaxis()->SetTitle("(StationEta - 1) * 2 + (Multiplet - 1)");
    h_resQuadMean->GetXaxis()->SetTitle("Sector");
    h_resQuadMean->Draw("text colz");
    c2.Print(outputDir + "2D_residualMap_mean.png");
    c2.Print(outputDir + "2D_residualMap_mean.svg");

    // Diff between the histograms
    c2.Clear();
    c2.SetRightMargin(0.10);
    c2.SetLeftMargin(0.12);
    c2.SetTopMargin(0.02);
    c2.SetBottomMargin(0.08);
    h_otResQuadSigma->Add(h_resQuadSigma, -1); 
    h_otResQuadSigma->GetYaxis()->SetTitle("(StationEta - 1) * 2 + (Multiplet - 1)");
    h_otResQuadSigma->GetXaxis()->SetTitle("Sector");
    h_otResQuadSigma->SetStats(0);
    h_otResQuadSigma->Draw("text colz");
    c2.Print(outputDir + "2D_diffResidual_sigma.png");
    c2.Print(outputDir + "2D_diffResidual_sigma.svg");
  } // End: plotting customized residual histograms 

  return 0;
}
