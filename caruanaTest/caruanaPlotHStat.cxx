
int plotOverlay (std::vector<TH1F> histos, TString histName, TString xlabel,
                 std::vector<TString> typeNames,
                 int nBins, float xmin, float xmax, float histMax,
                 float lmin_x, float lmax_x, float lmin_y, float lmax_y, bool drawNormal=false)
{
  TString plotDir = "plots";
  int histColor[] = {1, 2, 4, 12, 13};

  TLegend* leg = new TLegend(lmin_x, lmin_y, lmax_x, lmax_y);

  TCanvas c1("c1", "canvas", 800, 600);
  c1.SetRightMargin(0.02);
  c1.SetLeftMargin(0.12);
  c1.SetTopMargin(0.02);
  c1.SetBottomMargin(0.08);

  int nHist = static_cast<int>(histos.size());
  std::vector<TF1*> normalf;
  normalf.push_back(new TF1("normalWa", "14080*0.1*ROOT::Math::normal_pdf(x,1.,0.)", -5.0, 5.0));
  normalf.push_back(new TF1("normalCa", "14262*0.1*ROOT::Math::normal_pdf(x,1.,0.)", -5.0, 5.0));
  //
  std::vector<TPaveStats*> paveStats;
  for (int i = 0; i < nHist; ++i) {
    TString normalName = TString::Format("normal%d", i);
    //normalf.push_back(new TF1(normalName, "gaus", -5.0, 5.0));
    paveStats.push_back(new TPaveStats());
  }

  TH1F h1("h1", "", nBins, xmin, xmax);
  h1.SetMaximum(histMax);
  h1.GetYaxis()->SetTitle("Entries");
  h1.GetXaxis()->SetTitle(xlabel);
  h1.SetLineWidth(1);
  h1.SetLineColor(0);
  h1.Draw("HIST");
  for (int i = 0; i < nHist; ++i) {
    histos[i].SetLineWidth(2);
    histos[i].SetLineColor(histColor[i]);
    //if (drawNormal) histos[i].Scale(1.0 / histos[i].Integral());
    leg->AddEntry(&histos[i], typeNames[i], "l");
    histos[i].Draw("HIST SAMES");

    c1.Update();
    paveStats[i] = (TPaveStats*)histos[i].GetListOfFunctions()->FindObject("stats");
    paveStats[i]->SetName(typeNames[i]);
    paveStats[i]->SetY1NDC(paveStats[i]->GetY1NDC() - i*0.18);
    paveStats[i]->SetY2NDC(paveStats[i]->GetY2NDC() - i*0.18);
    //paveStats[i]->SetTextColor(1);
    paveStats[i]->SetTextColor(histColor[i]);
    paveStats[i]->Draw();
    c1.Update();

    if (drawNormal && i==1) {
      float npoint = histos[i].Integral(1,histos[i].GetNbinsX());
      float npAll = npoint + histos[i].GetBinContent(0) + histos[i].GetBinContent(histos[i].GetNbinsX()+1);
      std::cout << "..get total points " << typeNames[i] << ": "  << npoint << " All: " << npAll  << std::endl;
      //std::cout << "..fitting " << typeNames[i] << " below" << std::endl;
      normalf[i]->SetLineColor(histColor[i]);
      normalf[i]->Draw("SAME");
    }
  }
  //c1.Update();
  leg->Draw();
  c1.Print(plotDir + "/" + histName + ".png");

  delete leg;
  for (int i = 0; i < nHist; ++i) {
    delete normalf[i];
    delete paveStats[i];
  }

  return 0;
}

int caruanaPlotHStat () {

  // Plotting style
  gStyle->SetTitleYOffset(1.4);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  //gStyle->SetOptStat(0);

  std::cout << "... macro starting" << std::endl;

  // Plot objects
  TString type[] = {"wa", "ca250"};
  std::vector<TString> typeDetail = {"WA", "Caruana250"};
  const int nType = sizeof(type) / sizeof(type[0]);
  //std::cout << "...Processing " << nType << " clustering techniques" << std::endl;

  std::vector<TH1F> vh_residualTh;
  std::vector<TH1F> vh_residualTrk;
  std::vector<TH1F> vh_residualTrkMS;
  std::vector<TH1F> vh_pullTh;
  std::vector<TH1F> vh_pullTrk;
  std::vector<TH1F> vh_pullTrkMS;
  std::vector<TH1F> vh_clError;
  std::vector<TH1F> vh_residualTh12;
  std::vector<TH1F> vh_residualTrk12;
  std::vector<TH1F> vh_pullTh12;
  std::vector<TH1F> vh_pullTrk12;
  std::vector<TH1F> vh_clError12;
  //
  std::vector<TH1F> vh_clSize;
  std::vector<TH1F> vh_clCharge;
  std::vector<TH1F> vh_clTime;
  std::vector<TH1F> vh_clSize12;
  std::vector<TH1F> vh_clCharge12;
  std::vector<TH1F> vh_clTime12;
  for (const TString& t: type) {
    vh_residualTh.push_back(TH1F(t+"_residualTruth", "", 80, -2., 2));
    vh_residualTrk.push_back(TH1F(t+"_residualTrack", "", 80, -2., 2));
    vh_residualTrkMS.push_back(TH1F(t+"_residualTrackMS", "", 80, -2., 2));
    vh_pullTh.push_back(TH1F(t+"_pullTruth", "", 100, -5., 5.));
    vh_pullTrk.push_back(TH1F(t+"_pullTrack", "", 100, -5., 5.));
    vh_pullTrkMS.push_back(TH1F(t+"_pullTrackMS", "", 100, -5., 5.));
    vh_clError.push_back(TH1F(t+"_clerror", "", 100, 0., 1.0));
    //
    vh_residualTh12.push_back(TH1F(t+"_residualTruth1", "", 80, -2., 2));
    vh_residualTrk12.push_back(TH1F(t+"_residualTrack1", "", 80, -2., 2));
    vh_pullTh12.push_back(TH1F(t+"_pullTruth1", "", 100, -5., 5.));
    vh_pullTrk12.push_back(TH1F(t+"_pullTrack1", "", 100, -5., 5.));
    vh_clError12.push_back(TH1F(t+"_clerror1", "", 100, 0., 1.0));
    //
    vh_clSize.push_back(TH1F(t+"_clusterSize", "", 20, 0., 20));
    vh_clCharge.push_back(TH1F(t+"_clusterCharge", "", 200, 0., 1000.));
    vh_clTime.push_back(TH1F(t+"_clusterTime", "", 105, -90., 120.));
    vh_clSize12.push_back(TH1F(t+"_clusterSize1", "", 20, 0., 20));
    vh_clCharge12.push_back(TH1F(t+"_clusterCharge1", "", 200, 0., 1000.));
    vh_clTime12.push_back(TH1F(t+"_clusterTime1", "", 105, -90., 120.));
  }

  TString filename[] = {"/eos/user/c/chchau/sw/devArea/nswMCSamples/datasets/data_00440613_lb1000To1099_WeightedAvg.2023_02_06.NTUP_CAF_EXT0.root",
                        "/eos/user/c/chchau/sw/devArea/nswMCSamples/datasets/data_00440613_lb1000To1099_Carauna.2023_03_06.AddError_0.250.NTUP_CAF.root"};

  const int nFile = sizeof(filename) / sizeof(filename[0]);
  std::cout << "...Processing " << nType << " clustering techniques and " << nFile << " files" << std::endl;
  for (int f = 0; f < nFile; ++f) {
    TFile* file = new TFile(filename[f]);
    TTree* tree = (TTree*) file->Get("BasicTesterTree");
    TTreeReader reader;
    reader.SetTree(tree);
    std::cout << " ...filename: " << filename[f] << std::endl;

    TTreeReaderArray<float> muons_pt  (reader, "muons_pt");
    TTreeReaderArray<float> muons_eta (reader, "muons_eta");
    TTreeReaderArray<float> muons_phi (reader, "muons_phi");
    TTreeReaderArray<float> muons_e   (reader, "muons_e");
    TTreeReaderArray<int>   muons_q   (reader, "muons_q");
    TTreeReaderArray<unsigned short>   muons_author   (reader, "muons_author");
    TTreeReaderArray<unsigned short>   muons_type   (reader, "muons_type");
    //
    TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink     (reader, "stgcOnTrack_MuonLink");
    TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex  (reader, "stgcOnTrack_stationIndex");
    TTreeReaderArray<char> stgcOnTrack_stationEta             (reader, "stgcOnTrack_stationEta");
    TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi    (reader, "stgcOnTrack_stationPhi");
    TTreeReaderArray<unsigned char> stgcOnTrack_multiplet     (reader, "stgcOnTrack_multiplet");
    TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap       (reader, "stgcOnTrack_gas_gap");
    TTreeReaderArray<unsigned char> stgcOnTrack_channel_type  (reader, "stgcOnTrack_channel_type");
    TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
    TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
    TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
    TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
    TTreeReaderArray<int> stgcOnTrackNStrips      (reader, "stgcOnTrackNStrips");
    TTreeReaderArray<float> stgcOnTrackPullTrack       (reader, "stgcOnTrackPullTrack");
    TTreeReaderArray<float> stgcOnTrackPullTrackMS     (reader, "stgcOnTrackPullTrackMS");
    TTreeReaderArray<float> stgcOnTrackPullTruth       (reader, "stgcOnTrackPullTruth");
    TTreeReaderArray<float> stgcOnTrackResidualTrack   (reader, "stgcOnTrackResidualTrack");
    TTreeReaderArray<float> stgcOnTrackResidualTrackMS (reader, "stgcOnTrackResidualTrackMS");
    TTreeReaderArray<float> stgcOnTrackResidualTruth   (reader, "stgcOnTrackResidualTruth");
    TTreeReaderArray<float> stgcOnTrackError           (reader, "stgcOnTrackError");
    TTreeReaderArray<vector<int>> stgcOnTrackStripCharges      (reader, "stgcOnTrackStripCharges");
    TTreeReaderArray<vector<short>> stgcOnTrackStripDriftTimes       (reader, "stgcOnTrackStripDriftTimes");
    TTreeReaderArray<vector<unsigned short>> stgcOnTrackStripNumbers (reader, "stgcOnTrackStripNumbers");

    int count{0};
    while (reader.Next()) {

      //if (count > 2000) break;
      //if (count > 10) break;

      int nOT = stgcOnTrack_channel.GetSize();
      for (int i = 0; i < nOT; ++i) {
        int channelTypeOT = static_cast<int>(stgcOnTrack_channel_type[i]);
        int nStrip = static_cast<int>(stgcOnTrackNStrips[i]);
        if (channelTypeOT == 1) {
          vh_residualTrk.at(f).Fill(stgcOnTrackResidualTrack[i]);
          vh_residualTrkMS.at(f).Fill(stgcOnTrackResidualTrackMS[i]);
          vh_pullTrk.at(f).Fill(stgcOnTrackPullTrack[i]);
          vh_pullTrkMS.at(f).Fill(stgcOnTrackPullTrackMS[i]);
          vh_clError.at(f).Fill(TMath::Sqrt(stgcOnTrackError[i]));

          int cl_time{0}, cl_totalCharge{0};
          int maxCharge{0};
          std::vector<short> v_clTime = stgcOnTrackStripDriftTimes[i];
          std::vector<int> v_clTotalCharge = stgcOnTrackStripCharges[i];
          for (size_t j = 0; j < v_clTotalCharge.size(); ++j) {
            cl_totalCharge += v_clTotalCharge[j];
            if (v_clTotalCharge[j] > maxCharge) {
              maxCharge = v_clTotalCharge[j];
              cl_time = v_clTime.at(j);
            }
          }
          vh_clCharge.at(f).Fill(cl_totalCharge);
          vh_clTime.at(f).Fill(cl_time);
          vh_clSize.at(f).Fill(stgcOnTrackNStrips[i]);

          // Get muon pt
          float muPt = muons_pt[stgcOnTrack_MuonLink[i]];
          int muAuthor = static_cast<int>(muons_author[stgcOnTrack_MuonLink[i]]); 
          int muType = static_cast<int>(muons_type[stgcOnTrack_MuonLink[i]]); 
          if (muPt > 15.0 && (muAuthor==1 || muAuthor==5)) {
            vh_residualTrk12.at(f).Fill(stgcOnTrackResidualTrack[i]);
            vh_pullTrk12.at(f).Fill(stgcOnTrackPullTrack[i]);
            vh_clError12.at(f).Fill(TMath::Sqrt(stgcOnTrackError[i]));

            vh_clCharge12.at(f).Fill(cl_totalCharge);
            vh_clTime12.at(f).Fill(cl_time);
            vh_clSize12.at(f).Fill(stgcOnTrackNStrips[i]);
          }
        }
      }

      ++count;
    }

    delete file;
  }

  plotOverlay (vh_residualTrk, "residualTrack", "Track residual [mm]",
               typeDetail, 80, -2.0, 2.0, 6000, 0.2, 0.38, 0.8, 0.92);
  plotOverlay (vh_residualTrkMS, "residualTrackMS", "Track MS residual [mm]",
               typeDetail, 80, -2.0, 2.0, 6000, 0.2, 0.38, 0.8, 0.92);
  plotOverlay (vh_pullTrk, "pullTrack", "Track pull [mm]",
               typeDetail, 100, -5, 5, 18000, 0.2, 0.38, 0.8, 0.92,false);
  plotOverlay (vh_pullTrkMS, "pullTrackMS", "Track MS pull [mm]",
               typeDetail, 100, -5, 5, 5000, 0.2, 0.38, 0.8, 0.92, false);
  plotOverlay (vh_clError, "clusterError", "Error on the mean [mm]",
               typeDetail, 100, 0., 1.0, 10000, 0.2, 0.38, 0.8, 0.92);
  plotOverlay (vh_clSize, "clusterSize", "Cluster size",
               typeDetail, 20, 0., 20.0, 60000, 0.6, 0.78, 0.82, 0.935);
  plotOverlay (vh_clCharge, "clusterCharge", "Cluster total charge [fC]",
               typeDetail, 200, 0., 1000.0, 2000, 0.6, 0.78, 0.82, 0.935);
  plotOverlay (vh_clTime, "clusterTime", "Cluster Time [ns]",
               typeDetail, 210, -90., 120.0, 10000, 0.6, 0.78, 0.82, 0.935);

  plotOverlay (vh_residualTrk12, "residualTrack15", "muPt > 15, Track residual [mm]",
               typeDetail, 80, -2.0, 2.0, 4000, 0.2, 0.38, 0.8, 0.92);
  plotOverlay (vh_pullTrk12, "pullTrack15", "muPt > 15, Track pull [mm]",
               typeDetail, 100, -5, 5, 5000, 0.2, 0.38, 0.8, 0.92, false);
  plotOverlay (vh_clError12, "clusterError15", "muPt > 15, Error on the mean [mm]",
               typeDetail, 100, 0., 1.0, 6000, 0.2, 0.38, 0.8, 0.92);
  plotOverlay (vh_clSize12, "clusterSize15", "muPt > 15, Cluster size",
               typeDetail, 20, 0., 20.0, 30000, 0.6, 0.78, 0.82, 0.935);
  plotOverlay (vh_clCharge12, "clusterCharge15", "muPt > 15, Cluster total charge [fC]",
               typeDetail, 200, 0., 1000.0, 1000, 0.6, 0.78, 0.82, 0.935);
  plotOverlay (vh_clTime12, "clusterTime15", "muPt > 15, Cluster Time [ns]",
               typeDetail, 210, -90., 120.0, 6000, 0.6, 0.78, 0.82, 0.935);

  return 0;
}

