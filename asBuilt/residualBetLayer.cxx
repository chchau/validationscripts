
/// Calculate the residual between a layer and a reference layer
/// in the same quadruplet. The reference layer is the layer on which
/// the alignment platforms are installed, so it is either:
///   - layer 4 for multiplet 1 or
///   - layer 1 for multiplet 2.
/// The calculation is done as follow:
///   - Select the clusters on track associated to a good muon.
///   - Fit the selected clusters in a quad to straight line.
///   - Compute the distance between a cluster on a layer to the
///     cluster on the reference layer using the angle obtained 
///     from the best-fitted line.

// Channel type: Pad = 0, Strip = 1, Wire = 2
enum CHTYPE {PAD=0, STRIP, WIRE};

// Number of layers
const int NLAYER = 8;
const int NSECTOR = 16;
const int NSTATIONETA = 6;
const int NMLT = 2;
// Reference layer on multiplet 1
const int MLT1_REFLAYER = 4;
// Reference layer on multiplet 2
const int MLT2_REFLAYER = 1;

// Object containing the associated muon index and on-track cluster indices
struct Track {
  int id{-1};
  int muIndex{-1};
  // Channel type: Pad = 0, Strip = 1, Wire = 2
  std::array<std::vector<int>, 3> otIndices;

  Track() : id(-1), muIndex(-1) {}
  Track(int trkId, int muId) : id(trkId), muIndex(muId) {}

  void insertOT(int channelType, int index) {
    otIndices[channelType].push_back(index);
  }
};

struct Hit {
  int m_sector{0};
  int m_stationEta{0};
  int m_layer{0};
  int m_channelType{-1};
  int m_channel{0};
  int m_charge{0};
  int m_time{0};
  float m_posX{0.};
  float m_posY{0.};
  float m_posZ{0.};

  Hit() {}

  Hit(int sector, int stEta, int layer, int chType, int channel,
      int charge, int time, float posX, float posY, float posZ) :
            m_sector(sector),
            m_stationEta(stEta),
            m_layer(layer),
            m_channelType(chType),
            m_channel(channel),
            m_charge(charge),
            m_time(time),
            m_posX(posX),
            m_posY(posY),
            m_posZ(posZ) {
  }

  std::array<float, 3> position() const {
    std::array<float, 3> pos{m_posX, m_posY, m_posZ};
    return pos;
  }

  void setPosition(float x, float y, float z) {
    m_posX = x;
    m_posY = y;
    m_posZ = z;
  }

  bool isValid() const {
    if (m_sector > 0 && m_sector < 17) return true;
    return false;
  }
};

struct Residual {
  int m_sector{0};
  int m_stationEta{0};
  int m_layer{0};
  int m_channelType{-1};
  float m_residualIncl{-99.9};
  float m_residualExcl{-99.9};

  Residual() {}

  Residual(int sec, int stEta, int lay, int chType, float resIncl, float resExcl) :
          m_sector(sec),
          m_stationEta(stEta),
          m_layer(lay),
          m_channelType(chType),
          m_residualIncl(resIncl),
          m_residualExcl(resExcl) {
  }
};

struct TagHisto {
  int m_sector{0};
  int m_stationEta{0};
  int m_multiplet{0};
  std::array<TH1F*, 4> m_hLayerIncl{nullptr, nullptr, nullptr, nullptr};
  std::array<TH1F*, 4> m_hLayerExcl{nullptr, nullptr, nullptr, nullptr};

  TagHisto() {}

  TagHisto(int sec, int stEta, int mlt)
    : m_sector(sec),
      m_stationEta(stEta),
      m_multiplet(mlt)
  {
  }

  ~TagHisto() {
    if (!m_hLayerIncl.empty()) {
      for (TH1F* h: m_hLayerIncl) {
        if (h) {delete h; h = nullptr;}
      }
    }

    if (!m_hLayerExcl.empty()) {
      for (TH1F* h: m_hLayerExcl) {
        if (h) {delete h; h = nullptr;}
      }
    }
  }

  bool isSameHisto(int sec, int stEta, int mlt) {
    return (sec == m_sector && stEta == m_stationEta && mlt == m_multiplet);
  }

  void setIdentifier(int sec, int stEta, int mlt) {
    m_sector = sec;
    m_stationEta = stEta;
    m_multiplet = mlt;
  }

  bool addIdentifier(int sec, int stEta, int mlt) {
    if (m_sector == 0 || m_stationEta == 0 || m_multiplet == 0) {
      setIdentifier(sec, stEta, mlt);
      return true;
    } else {
      return isSameHisto(sec, stEta, mlt);
    }
  }
};

namespace Util {
  int findMuonTrack(const std::vector<Track>& muonTracks, int muIndex) {
    for (size_t i = 0; i < muonTracks.size(); ++i) {
      if (muIndex == muonTracks.at(i).muIndex) {
        return static_cast<int>(i);
      }
    }
    return -1;
  }
  
  int sector(int stationIndex, int stationPhi) {
    return (stationPhi * 2 - (stationIndex - 57));
  }

  int layer(int multiplet, int gasgap) {
    return (multiplet - 1) * 4 + gasgap;
  }

  int quadIndex(int sector, int stationEta, int multiplet) {
    int linearEta = (stationEta > 0)? stationEta : std::abs(stationEta)+3;
    return (sector - 1) * NSTATIONETA * NMLT + (linearEta - 1) * NMLT + (multiplet - 1);
  }

  // Fit a set of points <x, y> to a straight line
  // Return: the result array has four elements, where
  //        [0]: intercept, [2]: error on the intercept
  //        [1]: slope, [3]: error on the slope
  std::array<double,4> straightLineFit(std::vector<Hit> quadHits) {
    std::array<double,4>  result = {0, 0, 0, 0};
    const int nLayer = static_cast<int>(quadHits.size());

    // Not sufficent information for fitting
    if (nLayer < 2) return result;

    float posZ[nLayer];
    float posR[nLayer];

    for (int i = 0; i < nLayer; ++i) {
      posZ[i] = quadHits.at(i).m_posZ;
      posR[i] = quadHits.at(i).m_posX;
    }

    TGraph* graph = new TGraph(nLayer, posZ, posR); 
    TFitResultPtr fitRes = graph->Fit("pol1", "SQ");
    result[0] = fitRes->Parameter(0);
    result[1] = fitRes->Parameter(1);
    result[2] = fitRes->ParError(0);
    result[3] = fitRes->ParError(1);

    delete graph;
    return result;
  }

  bool plotResidualPerLayer(std::array<TH1F*, 4>& arrHisto, TString outputDir, TString histLabel) {
    // Plot canvas
    TCanvas c1 ("c1", "canvas", 800, 600);
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.12);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);

    // Dummy histo to define the plotting area
    TH1F* h_dummy = (TH1F*)arrHisto[0]->Clone("dummy");
    h_dummy->SetStats(0);
    TString yLabel = "Entries of " + histLabel.ReplaceAll("_", " ");
    h_dummy->GetXaxis()->SetTitle("Residual per layer [mm]");
    h_dummy->GetYaxis()->SetTitle(yLabel);
    // Find maximum
    float hist_max = arrHisto[0]->GetMaximum();
    for (int g = 1; g < 4; ++g) {
      float tmp_max = arrHisto[g]->GetMaximum();
      if (tmp_max > hist_max) hist_max = tmp_max;
    }
    hist_max = std::ceil((1.1 * hist_max) / 10) * 10.0;
    h_dummy->SetMaximum(hist_max);
    h_dummy->SetLineColor(0);
    h_dummy->SetMarkerColor(0);
    h_dummy->Draw();

    int linecolor[4] = {1, 633, 601, 417};
    // Overlay histograms
    for (int g = 0; g < 4; ++g) {
      arrHisto[g]->SetLineColor(linecolor[g]);
      arrHisto[g]->SetLineWidth(2);
      arrHisto[g]->Draw("SAME HIST");
    }

    // Print to a file
    Int_t msgLevel = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kWarning;
    c1.Print(outputDir + "lay-" + histLabel.ReplaceAll(" ", "_") + ".png");
    gErrorIgnoreLevel = msgLevel;

    delete h_dummy;
    return true;
  }
}

int residualBetLayer() {

  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit();

  bool plotHistogram = true;
  bool enableResidualPerLayer = true;
  bool enableSpreadsheet = true;

  TString outputDir = "plots/";
  //TString inDatasetPath = "/eos/atlas/atlasgroupdisk/det-muon/dq2/rucio/group/det-muon/";
  //TString datasetListName = "ds_440613asBuiltOnlyTranslation.txt";
  //TString inDatasetPath = "/eos/atlas/atlastier0/rucio/data23_13p6TeV/physics_Main/00454222/data23_13p6TeV.00454222.physics_Main.merge.NTUP_MUTEST.f1360_m2180_c1404_m2100/";
  //TString datasetListName = "ds_454222.txt";
  TString inDatasetPath = "/eos/atlas/atlasgroupdisk/det-muon/dq2/rucio/data22_13p6TeV/";
  TString datasetListName = "ds_440613f1325.txt";
  std::ifstream inDatasetFile;
  inDatasetFile.open(datasetListName);
  std::vector<TString> datasetList;

  if (inDatasetFile.is_open()) {
    std::string line;
    while (std::getline(inDatasetFile, line)) {
      if (line[0] == '#') continue;
      datasetList.push_back(line.c_str());
    }
    inDatasetFile.close(); // CLose input file
  } else {
    std::cout << "Error  Fail to read the input files" << std::endl;
    return 1;
  }
  std::cout << "...Reading the following root files" << std::endl;
  for (auto f: datasetList) {
    std::cout << f << std::endl;
  }

  TChain* fChain = new TChain("BasicTesterTree");
  //std::cout << "...Processing the following root files" << std::endl;
  for (auto ds: datasetList) {
    TString filename = inDatasetPath + ds;
    fChain->Add(filename);
    //std::cout << filename << std::endl;
  }
  TTreeReader reader;
  reader.SetTree(fChain);

  TTreeReaderArray<float> muons_pt   (reader, "muons_pt");
  TTreeReaderArray<float> muons_eta  (reader, "muons_eta");
  TTreeReaderArray<float> muons_phi  (reader, "muons_phi");
  TTreeReaderArray<float> muons_e    (reader, "muons_e");
  TTreeReaderArray<int> muons_q      (reader, "muons_q");
  TTreeReaderArray<unsigned short> muons_author  (reader, "muons_author");
  //
  TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink      (reader, "stgcOnTrack_MuonLink");
  TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
  TTreeReaderArray<unsigned char> stgcOnTrack_channel_type      (reader, "stgcOnTrack_channel_type");
  TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap      (reader, "stgcOnTrack_gas_gap");
  TTreeReaderArray<unsigned char> stgcOnTrack_multiplet      (reader, "stgcOnTrack_multiplet");
  TTreeReaderArray<char> stgcOnTrack_stationEta      (reader, "stgcOnTrack_stationEta");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex      (reader, "stgcOnTrack_stationIndex");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi      (reader, "stgcOnTrack_stationPhi");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
  TTreeReaderArray<int>   stgcOnTrackNStrips       (reader, "stgcOnTrackNStrips");
  TTreeReaderArray<float> stgcOnTrackPullTrack     (reader, "stgcOnTrackPullTrack");
  TTreeReaderArray<float> stgcOnTrackPullTruth     (reader, "stgcOnTrackPullTruth");
  TTreeReaderArray<float> stgcOnTrackResidualTrack (reader, "stgcOnTrackResidualTrack");
  TTreeReaderArray<float> stgcOnTrackResidualTruth (reader, "stgcOnTrackResidualTruth");
  TTreeReaderArray<float> stgcOnTrackLocalPos_x     (reader, "stgcOnTrackLocalPos_x");
  TTreeReaderArray<float> stgcOnTrackLocalPos_y     (reader, "stgcOnTrackLocalPos_y");
  TTreeReaderArray<float> stgcOnTrackError         (reader, "stgcOnTrackError");
  TTreeReaderArray<vector<int>> stgcOnTrackStripCharges = {reader, "stgcOnTrackStripCharges"};
  TTreeReaderArray<vector<short>> stgcOnTrackStripDriftTimes = {reader, "stgcOnTrackStripDriftTimes"};
  TTreeReaderArray<vector<unsigned short>> stgcOnTrackStripNumbers = {reader, "stgcOnTrackStripNumbers"};

  // Container to store the residuals
  std::vector<Residual> residuals;

  std::cout  << "...starting loop" << std::endl;
  int nEvent{0};
  while (reader.Next()) {
    //if (nEvent >= 500000) break;

    // muon tracks in a event
    std::vector<Track> muonTracks;

    int nMuon = muons_pt.GetSize();
    for (int i = 0; i < nMuon; ++i) {
      Track muTrk(i, i);
      muonTracks.push_back(std::move(muTrk));
    }

    ///** Find the indices of OnTrack hits associated to the muon track
    int nOT = stgcOnTrack_channel.GetSize();
    std::vector<int> badOTIndices;
    for (int i = 0; i < nOT; ++i) {
      int muIndex = static_cast<int>(stgcOnTrack_MuonLink[i]);
      int channelType = static_cast<int>(stgcOnTrack_channel_type[i]);
      int trkIndex = Util::findMuonTrack(muonTracks, muIndex);
      if (trkIndex >= 0) {
        muonTracks.at(trkIndex).insertOT(channelType, i);
      } else {
        badOTIndices.push_back(i);
      }
    } // End for-loop over on-track hits

    if (!badOTIndices.empty()) {
      std::cout << "badOTIndices: ";
      for (auto v: badOTIndices) 
        std::cout << "{" << v << "} ";
      std::cout << std::endl;
    }

    ///******************************************************
    /// For each muon track, separate the strip clusters by
    /// layer, verify that at least three layers in the same
    /// quadruplet have a cluster and fit a straight line to
    /// the clusters in order to compute the residuals.

    for (const Track& t: muonTracks) {

      int muPt = muons_pt[t.muIndex];
      int muAuthor = muons_author[t.muIndex];
      if (muPt < 15. || (muAuthor != 1 && muAuthor != 5)) continue;

      Hit emptyHit;
      std::array<Hit,NLAYER> hitArray = {emptyHit, emptyHit, emptyHit, emptyHit,
                                         emptyHit, emptyHit, emptyHit, emptyHit};

      int multipleHitPerLayer[NLAYER] = {0, 0, 0, 0, 0, 0, 0, 0};
      // Loop over the strip clusters associated to the muon and
      // assign clusters to a array of eight elements
      for (int i: t.otIndices[STRIP]) {
        int stIndex = static_cast<int>(stgcOnTrack_stationIndex[i]);
        int stEta = static_cast<int>(stgcOnTrack_stationEta[i]);
        int stPhi = static_cast<int>(stgcOnTrack_stationPhi[i]);
        int mlt = static_cast<int>(stgcOnTrack_multiplet[i]);
        int gap = static_cast<int>(stgcOnTrack_gas_gap[i]);
        int chType = static_cast<int>(stgcOnTrack_channel_type[i]);
        int ch = static_cast<int>(stgcOnTrack_channel[i]);
        float globalPosX = stgcOnTrackGlobalPos_x[i];
        float globalPosY = stgcOnTrackGlobalPos_y[i];
        float globalPosZ = stgcOnTrackGlobalPos_z[i];
        float localPosX = stgcOnTrackLocalPos_x[i];
        float localPosY = stgcOnTrackLocalPos_y[i];
        std::vector<int> charges = stgcOnTrackStripCharges[i];
        std::vector<short> times = stgcOnTrackStripDriftTimes[i];

        // Compute the cluster's charge and time
        int maxCharge{-1}, totalCharge{0};
        int time{0};
        for (size_t q = 0; q < charges.size(); ++q) {
          totalCharge += charges[q];
          if (charges[q] > maxCharge) {
            maxCharge = charges[q];
            time = times[q];
          }
        }

        int layer = Util::layer(mlt, gap);
        if (layer < 1 || layer > NLAYER) {
          std::cout << "Error got unexpected layer: " << layer << std::endl; 
          continue;
        }

        Hit hitCluster(Util::sector(stIndex, stPhi), stEta, layer, chType, ch,
                       totalCharge, time, localPosX, localPosY, globalPosZ);
                       //totalCharge, time, globalPosX, globalPosY, globalPosZ);
        if (!hitArray[layer-1].isValid()) {
          hitArray[layer-1] = std::move(hitCluster);
        } else {
          // Found more than one cluster on the same layer
          multipleHitPerLayer[layer-1] += 1;
          
          // choose the cluster with the greatest charge
          if (hitCluster.m_charge > hitArray[layer-1].m_charge) {
            hitArray[layer-1] = std::move(hitCluster);
          }
        }
      } // End for-loop over indices of on-track hits

      for (int i = 0; i < NLAYER; ++i) {
        if (multipleHitPerLayer[i] > 1)
          std::cout << "Info  " << "Layer: " << i << ", found " << multipleHitPerLayer[i] << " instances of multiple strip clusters per layer" << std::endl;
      }

      ///** Verify the clusters are inside the same quad, 
      ///   and at least three layers in each quad have a cluster

      bool isSameQuad[2] = {true, true};

      //int nLayerQuad1{0}, nLayerQuad2{0};
      int nLayerQuad[2] = {0, 0};
      int prevLayer{0};
      for (int iLayer = 1; iLayer < NLAYER; ++iLayer) {
        // Skip layer 5 that is the 1st layer of multiplet 2
        if (iLayer == 4) {
          prevLayer = 4;
          continue;
        }

        // Skip invalid layer
        if (!hitArray[iLayer].isValid()) continue;

        int iMlt = std::floor(iLayer/4); //index of the multiplet
        if (hitArray[prevLayer].isValid() && (std::floor(prevLayer/4) == iMlt)) {
          // Verify if both gaps are in the same quadruplet
          if (hitArray[iLayer].m_sector == hitArray[prevLayer].m_sector &&
              hitArray[iLayer].m_stationEta == hitArray[prevLayer].m_stationEta) {
            // Increment the number of layer by one, unless it is the first pair of layers
            nLayerQuad[iMlt] += 1;
            prevLayer = iLayer;
            if (nLayerQuad[iMlt] == 1) nLayerQuad[iMlt] += 1;
          } else {
            isSameQuad[iMlt] = false;
          }
        } else {
          // The current layer is the first valid layer
          prevLayer = iLayer;
        }
      } // End for-loop to count the number of valid layers in Quad1 and Quad 2


      ///** Compute the residual
      ///   Fit a straight line and compute the residual. Repeat for each valid layer in the quad
      int refLayer[2] = {MLT1_REFLAYER, MLT2_REFLAYER};
      for (int m = 0; m < 2; ++m) {
        // Skip multiplet having less than three layers
        if ((nLayerQuad[m] < 4) || (!isSameQuad[m])) continue;
        if (!hitArray[refLayer[m] - 1].isValid()) continue;

        std::vector<Hit> quadHits;
        int indBegin = (m == 0)? 0 : NLAYER/2;
        for (int iHit = indBegin; iHit < (indBegin + NLAYER/2); ++iHit) {
          if (hitArray[iHit].isValid()) quadHits.push_back(hitArray[iHit]);
        }

        int numberLayer = static_cast<int>(quadHits.size());
        //if (numberLayer < nLayerQuad[m]) {
        //  std::cout << "Error Residual calculation requires at least three layers, but got only " << numberLayer << " layers" << std::endl;
        //  continue;
        //}

        // Inclusive fit
        std::array<double,4> paramInclusiveFit = Util::straightLineFit(quadHits);
        if (std::abs(paramInclusiveFit[0]) < 0.001 &&  std::abs(paramInclusiveFit[1]) < 0.001) {
           std::cout << "Error  Inclusive fit fails due to p0: " << paramInclusiveFit[0] 
                     << ", p1: " << paramInclusiveFit[1] << std::endl;
           continue;
        }

        //residual
        for (int iHit = indBegin; iHit < (indBegin + NLAYER/2); ++iHit) {
          if (!hitArray[iHit].isValid()) continue;

          int indRefLayer = indBegin + refLayer[m] - 1;
          if (iHit == indRefLayer) continue;

          float diff_radius = hitArray[iHit].m_posX + paramInclusiveFit[1] * (hitArray[indRefLayer].m_posZ - hitArray[iHit].m_posZ)
                               - hitArray[indRefLayer].m_posX;
          // Save residuals
          Residual lay_res(hitArray[iHit].m_sector, hitArray[iHit].m_stationEta, hitArray[iHit].m_layer,
                           hitArray[iHit].m_channelType, diff_radius, 0.);
          residuals.push_back(std::move(lay_res));
        }

      }

    } // End for-loop over muon tracks

    ++nEvent;
  } // End while-loop over the number of events in the file
  std::cout << "Processed " << nEvent << " events" << std::endl;



  if (plotHistogram) {
    std::cout << "Plotting histograms" << std::endl;

    const float HMIN = -2.5;
    const float HMAX = 2.5;
    int HNBINS = static_cast<int>(HMAX - HMIN) / 0.05;
    TH1F* h_residualAll = new TH1F("residual", "", HNBINS, HMIN, HMAX);

    const int nQuad = NSECTOR * NSTATIONETA * NMLT;
    std::array<TagHisto, nQuad> hv_residualQuad;
    for (int i = 0; i < nQuad; ++i) {
      // Initialize the per-layer histos
      for (int g = 0; g < NLAYER/2; ++g) {
        TString histoName = TString::Format("resQuad%dL%d", i, g+1);
        hv_residualQuad[i].m_hLayerIncl[g] = new TH1F(histoName, "", HNBINS, HMIN, HMAX);
      }
    }

    for (const Residual& r: residuals) {
      if (r.m_sector > 0 && r.m_sector < 17) {
        h_residualAll->Fill(r.m_residualIncl);

        if (enableResidualPerLayer) {
          int mlt = (r.m_layer < 5)? 1 : 2;
          int ind_histo = Util::quadIndex(r.m_sector, r.m_stationEta, mlt);
          if (hv_residualQuad[ind_histo].addIdentifier(r.m_sector, r.m_stationEta, mlt)) {
            int ind_layer = r.m_layer - 4 * (mlt - 1) - 1;
            hv_residualQuad[ind_histo].m_hLayerIncl[ind_layer]->Fill(r.m_residualIncl);
          } else {
            std::cout << "Error  Not same histo" << std::endl;
            continue;
          }
        }
      }
    }

    Int_t oldLevel = gErrorIgnoreLevel;

    TCanvas c1 ("c1", "canvas", 800, 600);

    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.10);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
    h_residualAll->GetXaxis()->SetTitle("Residual [mm]");
    h_residualAll->GetYaxis()->SetTitle("Entries");
    //
    float hmean = h_residualAll->GetMean();
    float hrms = h_residualAll->GetRMS();
    TF1 *g1 = new TF1("gi1", "gaus", hmean-1.0*hrms, hmean+1.0*hrms);
    TF1 *g2 = new TF1("gi2", "gaus", HMIN, HMAX);
    TF1 *totalGaus = new TF1("totalGaus", "gaus(x,[0],[1],[2])+gaus(x,[3],[1],[4])", HMIN, HMAX);
    double fitParam[6];
    h_residualAll->Fit(g1, "RQ");
    h_residualAll->Fit(g2, "RQ");
    g1->GetParameters(&fitParam[0]);
    g2->GetParameters(&fitParam[3]);
    totalGaus->SetParameters(fitParam[0],fitParam[1],fitParam[2],fitParam[3],fitParam[5]);
    TFitResultPtr inclGaus = h_residualAll->Fit(totalGaus, "S", "", HMIN, HMAX);
    double sigma_incl = inclGaus->Parameter(2);
    //
    //h_residualAll->Draw("hist");
    gErrorIgnoreLevel = kWarning;
    c1.Print(outputDir + "residual_all.png");
    gErrorIgnoreLevel = oldLevel;
    delete g1; delete g2; delete totalGaus;

    std::cout << "Global resolution is: " << sigma_incl << std::endl;

    if (enableResidualPerLayer) {

      for (TagHisto& tagH : hv_residualQuad) {
        if ((tagH.m_sector < 1) || (tagH.m_sector > 16)) continue;

        TString histLabel = TString::Format("sector%d_stationEta%d_multiplet%d",
                                            tagH.m_sector, tagH.m_stationEta, tagH.m_multiplet);
        Util::plotResidualPerLayer(tagH.m_hLayerIncl, outputDir, "diffReference_" + histLabel);
      } // End of for-loop over hv_residualQuad

    } // End of if-statement to plot each layer

    std::cout << "Writing information to a spreadsheet: " << std::endl;
    if (enableSpreadsheet) {

      const auto default_precision {std::cout.precision()};
      std::ofstream resFile;
      resFile.open("layer_residual_info.dat", ios::out);
      for (TagHisto& tagH: hv_residualQuad) {
        if ((tagH.m_sector < 1) || (tagH.m_sector > 16)) continue;

        TString chamber_id = TString::Format("%d, %d, %d", tagH.m_sector,
                                                           tagH.m_stationEta,
                                                           tagH.m_multiplet);

        float refLayerMean{-1.0}, refLayerSigma{-1.0};
        int refLayerIndex = (tagH.m_multiplet == 1) ? MLT1_REFLAYER-1 : MLT2_REFLAYER-1;

        resFile << std::setprecision(3);
        for (int i = 0; i < 4; ++i) {
          
          float tmpMean{0.0};
          float tmpSigma{0.0};

          if ((i != refLayerIndex)) {

            tmpMean = tagH.m_hLayerIncl[i]->GetMean();
            tmpSigma = tagH.m_hLayerIncl[i]->GetRMS();
          }

          if ((tmpSigma > 0.) || (i != refLayerIndex)) {
            resFile << chamber_id << ", " << i+1 << ", " << tmpMean << ", " << tmpSigma;
          } else {
            resFile << chamber_id << ", " << i+1 << ", , ";
          }

          resFile << std::endl;
        }
      }
      resFile << std::setprecision(default_precision);
      resFile.close();

    } //End if-statement enableSpreadsheet

  }

  std::cout << "Application runs successfully..." << std::endl;
  return 0;
}
