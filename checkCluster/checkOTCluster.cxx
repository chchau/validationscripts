
constexpr int NSTRIP_QS1 = 307;
constexpr int NSTRIP_QS2 = 365;
constexpr int NSTRIP_QS3 = 406;
constexpr int NSTRIP_QL1 = 353;
constexpr int NSTRIP_QL2 = 366;
constexpr int NSTRIP_QL3 = 408;

constexpr float TIMEWINDOW[2] = {-87.5, 112.5};

struct Hit {
  int channel{0};
  int charge{0};
  int time{0};

  Hit(int ch, int q, int t) : channel(ch), charge(q), time(t) {}
};

struct Cluster {
  int stIndex{0};
  int stEta{0};
  int stPhi{0};
  int mlt{0};
  int gasgap{0};
  int muLink{-1};
  std::vector<Hit> hits;

  Cluster(int index, int eta, int phi, int mlt, int gap, int mulink) : stIndex(index),
      stEta(eta), stPhi(phi), mlt(mlt), gasgap(gap), muLink(mulink)
  {
  }
};

struct HistoCl {
  int stationIndex{0};
  int stationPhi{0};
  int multiplet{0};
  int gasgap{0};
  TH2F* h2f{0};

  bool isSameHisto(int stInd, int stPhi, int mlt, int gap) {
    return (stInd == stationIndex &&
            stPhi == stationPhi &&
            mlt == multiplet &&
            gap == gasgap);
  }

  void setIdentifier(int stInd, int stPhi, int mlt, int gap) {
    stationIndex = stInd;
    stationPhi = stPhi;
    multiplet = mlt;
    gasgap = gap;
  }

  bool addIdentifier(int stInd, int stPhi, int mlt, int gap) {
    if (stationIndex == 0 || stationPhi == 0 || multiplet == 0 || gasgap == 0) {
      setIdentifier(stInd, stPhi, mlt, gap);
      return true;
    } else {
     return isSameHisto(stInd, stPhi, mlt, gap);
    }
  }
};

struct Interval {
  float value{0.};
  int nEntries{0};
  float lowBound{0.};
  float upBound{0.};

  Interval(float val, int n, float lowBound, float upBound) 
      : value(val),
        nEntries(n),
        lowBound(lowBound),
        upBound(upBound)
  {
  }

  void addValue(float val) {value += val; ++nEntries;}
};

int sector(int stationIndex, int stationPhi) {
  //return (stationIndex == 57)? stationPhi * 2: stationPhi * 2 - 1;
  return (stationPhi * 2 - (stationIndex - 57));
}

int layer(int multiplet, int gasgap) {
  return (multiplet-1) * 4 + gasgap;
}

int sectorStripNumber(int stripNumber, int stationIndex, int stationEta) {
  int number = stripNumber;
  if (std::abs(stationEta) == 2) {
    if (stationIndex == 57) 
      number += NSTRIP_QS1;
    else 
      number += NSTRIP_QL1;
  } else if (std::abs(stationEta) == 3) {
    if (stationIndex == 57) 
      number += NSTRIP_QS1 + NSTRIP_QS2;
    else 
      number += NSTRIP_QL1 + NSTRIP_QL2;
  }

  return number;
}

int checkOTCluster() {

  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);

  TString plotDir = "plots/";

  std::cout << "... macro starting" << std::endl;
  TChain* fChain = new TChain("BasicTesterTree");
  fChain->Add("/data2/stgc2021/sw/digitStudy/validation/checkCluster/clsize/job12/nsw_stgc.root");
  TTreeReader reader;
  reader.SetTree(fChain);

  TTreeReaderArray<float> muons_pt   (reader, "muons_pt");
  TTreeReaderArray<float> muons_eta  (reader, "muons_eta");
  TTreeReaderArray<float> muons_phi  (reader, "muons_phi");
  TTreeReaderArray<float> muons_e    (reader, "muons_e");
  TTreeReaderArray<int> muons_q      (reader, "muons_q");
  TTreeReaderArray<unsigned short> muons_author  (reader, "muons_author");
  TTreeReaderArray<unsigned short> muons_type    (reader, "muons_type");
  //
  TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink      (reader, "stgcOnTrack_MuonLink");
  TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
  TTreeReaderArray<unsigned char> stgcOnTrack_channel_type      (reader, "stgcOnTrack_channel_type");
  TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap      (reader, "stgcOnTrack_gas_gap");
  TTreeReaderArray<unsigned char> stgcOnTrack_multiplet      (reader, "stgcOnTrack_multiplet");
  TTreeReaderArray<char> stgcOnTrack_stationEta      (reader, "stgcOnTrack_stationEta");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex      (reader, "stgcOnTrack_stationIndex");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi      (reader, "stgcOnTrack_stationPhi");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
  TTreeReaderArray<int>   stgcOnTrackNStrips       (reader, "stgcOnTrackNStrips");
  TTreeReaderArray<float> stgcOnTrackPullTrack     (reader, "stgcOnTrackPullTrack");
  TTreeReaderArray<float> stgcOnTrackPullTruth     (reader, "stgcOnTrackPullTruth");
  TTreeReaderArray<float> stgcOnTrackResidualTrack (reader, "stgcOnTrackResidualTrack");
  TTreeReaderArray<float> stgcOnTrackResidualTruth (reader, "stgcOnTrackResidualTruth");
  TTreeReaderArray<float> stgcOnTrackLocalPos_x     (reader, "stgcOnTrackLocalPos_x");
  TTreeReaderArray<float> stgcOnTrackLocalPos_y     (reader, "stgcOnTrackLocalPos_y");
  TTreeReaderArray<float> stgcOnTrackError         (reader, "stgcOnTrackError");
  TTreeReaderArray<vector<int>> stgcOnTrackStripCharges = {reader, "stgcOnTrackStripCharges"};
  TTreeReaderArray<vector<short>> stgcOnTrackStripDriftTimes = {reader, "stgcOnTrackStripDriftTimes"};
  TTreeReaderArray<vector<unsigned short>> stgcOnTrackStripNumbers = {reader, "stgcOnTrackStripNumbers"};
  //
  TTreeReaderArray<unsigned short> stgcBCID = {reader, "stgcBCID"};
  TTreeReaderArray<int> stgcCharge = {reader, "stgcCharge"};
  TTreeReaderArray<float> stgcError = {reader, "stgcError"};
  TTreeReaderArray<float> stgcGlobalPos_x = {reader, "stgcGlobalPos_x"};
  TTreeReaderArray<float> stgcGlobalPos_y = {reader, "stgcGlobalPos_y"};
  TTreeReaderArray<float> stgcGlobalPos_z = {reader, "stgcGlobalPos_z"};
  TTreeReaderArray<float> stgcLocalPos_x = {reader, "stgcLocalPos_x"};
  TTreeReaderArray<float> stgcLocalPos_y = {reader, "stgcLocalPos_y"};
  TTreeReaderArray<int> stgcNStrips = {reader, "stgcNStrips"};
  TTreeReaderArray<vector<unsigned short>> stgcStripChannel = {reader, "stgcStripChannel"};
  TTreeReaderArray<vector<int>> stgcStripCharge = {reader, "stgcStripCharge"};
  TTreeReaderArray<vector<short>> stgcStripTime = {reader, "stgcStripTime"};
  TTreeReaderArray<short> stgcTime = {reader, "stgcTime"};
  TTreeReaderArray<unsigned char> stgc_stationIndex = {reader, "stgc_stationIndex"};
  TTreeReaderArray<char>          stgc_stationEta   = {reader, "stgc_stationEta"};
  TTreeReaderArray<unsigned char> stgc_stationPhi   = {reader, "stgc_stationPhi"};
  TTreeReaderArray<unsigned char> stgc_multiplet    = {reader, "stgc_multiplet"};
  TTreeReaderArray<unsigned char> stgc_gas_gap      = {reader, "stgc_gas_gap"};
  TTreeReaderArray<unsigned char> stgc_channel_type = {reader, "stgc_channel_type"};

  // Cluster containers
  std::vector<Cluster> oneStripClusters;
  std::vector<Cluster> oneStripClusters15;
  std::vector<Cluster> twoStripClusters;
  std::vector<Cluster> twoStripClusters15;

  // histogram
  TH2F* h_1stripCl = new TH2F("oneStripClusters", "", 340, -17.0, 17.0, 1130, 0, 1130); 
  TH2F* h_1stripClMu = new TH2F("oneStripClustersMu", "", 340, -17.0, 17.0, 1130, 0, 1130); 
  TH2F* h_2stripCl = new TH2F("twoStripClusters", "", 340, -17.0, 17.0, 1130, 0, 1130); 
  TH2F* h_2stripClMu = new TH2F("twoStripClustersMu", "", 340, -17.0, 17.0, 1130, 0, 1130); 

  TH1F* h_1stripTime = new TH1F("oneStripTime", "", 45, -100.0, 125.0);
  TH1F* h_1stripTimeMu = new TH1F("oneStripTimeMu", "", 45, -100.0, 125.0);
  TH1F* h_1stripCharge = new TH1F("oneStripCharge", "", 150, 0.0, 1500.0);
  TH1F* h_1stripChargeMu = new TH1F("oneStripChargeMu", "", 150, 0.0, 1500.0);
  TH1F* h_2stripTime = new TH1F("twoStripTime", "", 45, -100.0, 125.0);
  TH1F* h_2stripTimeMu = new TH1F("twoStripTimeMu", "", 45, -100.0, 125.0);
  TH1F* h_2stripCharge = new TH1F("twoStripCharge", "", 150, 0.0, 1500.0);
  TH1F* h_2stripChargeMu = new TH1F("twoStripChargeMu", "", 150, 0.0, 1500.0);
  TH1F* h_nhole = new TH1F("numberHoles", "", 10, 0.0, 10.0);
  TH1F* h_nholeMu = new TH1F("numberHolesMu", "", 10, 0.0, 10.0);

  TString clHistoName[] = {"All", "Mu", "MuEG2", "MuEG3", "MuSmall", "MuLarge",
                           "MuW1", "MuW2", "MuW3", "MuW4", "MuW5", "MuW6", "MuW7", "MuW8",
                           "MuW9", "MuW10", "MuW11", "MuW12", "MuW13", "MuW14", "MuW15", "MuW16",
                           "MuAQL1", "MuAQL2", "MuAQL3", "MuAQS1", "MuAQS2", "MuAQS3",
                           "MuCQL1", "MuCQL2", "MuCQL3", "MuCQS1", "MuCQS2", "MuCQS3"};
  const int nClHisto = sizeof(clHistoName) / sizeof(clHistoName[0]);;
  std::cout << "... Number of average strip cluster size plots: " << nClHisto << std::endl;

  std::vector<TH2F*> v_clHistos;
  for (size_t i = 0; i < nClHisto; ++i) {
    TString strIndex = TString::Format("%d", static_cast<int>(i));
    TString hName = "clSizeTheta" + clHistoName[i] + strIndex;
    v_clHistos.push_back(new TH2F(hName, "", 70, -35.0, 35.0, 40, 0, 40)); 
  }

  const int nLayer = 128;
  std::vector<HistoCl> v_layerHistos;
  for (size_t i = 0; i < nLayer; ++i) {
    TString strIndex = TString::Format("%d", static_cast<int>(i));
    TString hName = "clSizeTheta" + strIndex;
    HistoCl histoCl;
    histoCl.h2f = new TH2F(hName, "", 70, -35.0, 35.0, 40, 0, 40); 
    v_layerHistos.push_back(histoCl); 
  }

  int nEvent{0};
  while (reader.Next()) {
    //if (nEvent > 1000) break;
    //std::cout << "...Starting to process event " << nEvent << std::endl;

    std::vector<Cluster> otClusters;
    int nOT = stgcOnTrack_channel.GetSize();
    for (int i = 0; i < stgcOnTrack_channel.GetSize(); ++i) {
      int stIndex = static_cast<int>(stgcOnTrack_stationIndex[i]);
      int stEta = static_cast<int>(stgcOnTrack_stationEta[i]);
      int stPhi = static_cast<int>(stgcOnTrack_stationPhi[i]);
      int mlt = static_cast<int>(stgcOnTrack_multiplet[i]);
      int gap = static_cast<int>(stgcOnTrack_gas_gap[i]);
      int chType = static_cast<int>(stgcOnTrack_channel_type[i]);
      int ch = static_cast<int>(stgcOnTrack_channel[i]);
      int secIndex = sector(stIndex, stPhi);
      bool isSmall = (stIndex == 57);

      int muIndex = static_cast<int>(stgcOnTrack_MuonLink[i]);

      if (chType == 1) {
        int nStrip = stgcOnTrackStripNumbers[i].size();

        // Evaluate the quality of the reconstructed muon
        bool passQuality = false;
        if (muons_pt[muIndex] > 15.0 && ((muons_author[muIndex]==1) || (muons_author[muIndex]==5))) {
          passQuality = true;
        }

        // Count the number of holes in a cluster
        int nhole{0}, nholeMu{0};
        for (int q: stgcOnTrackStripCharges[i]) {
          if (q == 0) {
            ++nhole;
            if (passQuality) ++nholeMu;
          }
        }
        h_nhole->Fill(nhole);
        h_nholeMu->Fill(nholeMu);

        // Retrieve muon theta
        float eta = muons_eta[stgcOnTrack_MuonLink[i]];
        float theta = 2 * TMath::ATan(TMath::Exp(-eta)) * 180.0 / TMath::Pi();

        // Cluster size vs theta
        float tmp_theta = (stEta > 0)? theta: -1*(180-theta);
        v_clHistos.at(0)->Fill(tmp_theta, nStrip);
        if (passQuality) {
          v_clHistos.at(1)->Fill(tmp_theta, nStrip);
          if (nStrip > 1) v_clHistos.at(2)->Fill(tmp_theta, nStrip);
          if (nStrip > 2) v_clHistos.at(3)->Fill(tmp_theta, nStrip);
          if (isSmall) { 
            v_clHistos.at(4)->Fill(tmp_theta, nStrip);
          } else {
            v_clHistos.at(5)->Fill(tmp_theta, nStrip);
          }

          // Index of histo for a specific sector is: 5+sector
          v_clHistos.at(5+secIndex)->Fill(tmp_theta, nStrip);

          // cluster size vs theta in specific eta modules
          int secEtaIndex = 5 + 16;
          if (stIndex==58 && stEta > 0) {
            secEtaIndex += TMath::Abs(stEta);
          } else if (stIndex==57 && stEta > 0) {
            secEtaIndex += 3 + TMath::Abs(stEta);
          } else if (stIndex==58 && stEta < 0) {
            secEtaIndex += 6 + TMath::Abs(stEta);
          } else if (stIndex==57 && stEta < 0) {
            secEtaIndex += 9 + TMath::Abs(stEta);
          }
          v_clHistos.at(secEtaIndex)->Fill(tmp_theta, nStrip);

          // Get cluster size vs theta for each layer
          int sectorLayIndex = (sector(stIndex, stPhi) - 1) * 8 + (layer(mlt, gap) - 1);
          if (v_layerHistos.at(sectorLayIndex).addIdentifier(stIndex, stPhi, mlt, gap)) {
            v_layerHistos.at(sectorLayIndex).h2f->Fill(tmp_theta, nStrip);
          } else {
            std::cout << " ERROR  Histogram at " << sectorLayIndex << " differs from sector: " << sector(stIndex, stPhi) << ", layer: " << layer(mlt, gap) << std::endl;
          }
        }

        if (nStrip == 1) {
          Cluster cluster(stIndex, stEta, stPhi, mlt, gap, muIndex);
          Hit stripHit(stgcOnTrackStripNumbers[i].at(0),
                       stgcOnTrackStripCharges[i].at(0),
                       stgcOnTrackStripDriftTimes[i].at(0));
          cluster.hits.push_back(std::move(stripHit));
          oneStripClusters.push_back(cluster);

          h_1stripTime->Fill(stgcOnTrackStripDriftTimes[i].at(0));
          h_1stripCharge->Fill(stgcOnTrackStripCharges[i].at(0));

          float xbin = (secIndex + 0.1 * layer(mlt, gap) + 0.01) * (stEta / std::abs(stEta));
          float ybin = sectorStripNumber(stgcOnTrackStripNumbers[i].at(0), stIndex, stEta) + 0.1;
          h_1stripCl->Fill(xbin, ybin, 1);

          if (passQuality) {
            h_1stripTimeMu->Fill(stgcOnTrackStripDriftTimes[i].at(0));
            h_1stripChargeMu->Fill(stgcOnTrackStripCharges[i].at(0));

            oneStripClusters15.push_back(cluster);
            h_1stripClMu->Fill(xbin, ybin, 1);
          }

        } else if (nStrip == 2) {
          Cluster cluster(stIndex, stEta, stPhi, mlt, gap, muIndex);
          cluster.hits.push_back(Hit(stgcOnTrackStripNumbers[i].at(0),
                                     stgcOnTrackStripCharges[i].at(0),
                                     stgcOnTrackStripDriftTimes[i].at(0)) );
          cluster.hits.push_back(Hit(stgcOnTrackStripNumbers[i].at(1),
                                     stgcOnTrackStripCharges[i].at(1),
                                     stgcOnTrackStripDriftTimes[i].at(1)) );
          twoStripClusters.push_back(cluster);

          float timeAverage = (stgcOnTrackStripDriftTimes[i].at(0) * stgcOnTrackStripCharges[i].at(0) +
                               stgcOnTrackStripDriftTimes[i].at(1) * stgcOnTrackStripCharges[i].at(1)) /
                                 (stgcOnTrackStripCharges[i].at(0) + stgcOnTrackStripCharges[i].at(1));
          h_2stripTime->Fill(timeAverage);

          float totalCharge = (stgcOnTrackStripCharges[i].at(0) + stgcOnTrackStripCharges[i].at(1));
          h_2stripCharge->Fill(totalCharge);

          float xbin = (secIndex + 0.1 * layer(mlt, gap) + 0.01) * (stEta / std::abs(stEta));
          float ybin1 = sectorStripNumber(stgcOnTrackStripNumbers[i].at(0), stIndex, stEta) + 0.1;
          float ybin2 = sectorStripNumber(stgcOnTrackStripNumbers[i].at(1), stIndex, stEta) + 0.1;
          h_2stripCl->Fill(xbin, ybin1, 1);
          h_2stripCl->Fill(xbin, ybin2, 1);

          if (passQuality) {
            h_2stripTimeMu->Fill(timeAverage);
            h_2stripChargeMu->Fill(totalCharge);

            twoStripClusters.push_back(cluster);
            h_2stripClMu->Fill(xbin, ybin1, 1);
            h_2stripClMu->Fill(xbin, ybin2, 1);
          }
        }
      }

    } // end  for-loop over onTrack hits

    ++nEvent;
  }
  std::cout << "Done processing " << nEvent << " events ..." << std::endl;

  TCanvas c1 ("c1", "canvas", 800, 600);

  h_1stripTime->GetXaxis()->SetTitle("Strip time [ns]");
  h_1stripTime->GetYaxis()->SetTitle("Entries");
  h_1stripTime->SetStats(0);
  h_1stripTime->Draw("HITS");
  c1.Print(plotDir + "oneStripTime.png");
  c1.Clear();
  h_1stripTimeMu->GetXaxis()->SetTitle("Strip time [ns]");
  h_1stripTimeMu->GetYaxis()->SetTitle("Entries");
  h_1stripTimeMu->SetStats(0);
  h_1stripTimeMu->Draw("HITS");
  c1.Print(plotDir + "oneStripTime_selection.png");

  c1.Clear();
  h_2stripTime->GetXaxis()->SetTitle("Strip time [ns]");
  h_2stripTime->GetYaxis()->SetTitle("Entries");
  h_2stripTime->SetStats(0);
  h_2stripTime->Draw("HITS");
  c1.Print(plotDir + "twoStripTime.png");
  c1.Clear();
  h_2stripTimeMu->GetXaxis()->SetTitle("Strip time [ns]");
  h_2stripTimeMu->GetYaxis()->SetTitle("Entries");
  h_2stripTimeMu->SetStats(0);
  h_2stripTimeMu->Draw("HITS");
  c1.Print(plotDir + "twoStripTime_selection.png");

  c1.Clear();
  h_1stripCharge->GetXaxis()->SetTitle("Strip charge [fC]");
  h_1stripCharge->GetYaxis()->SetTitle("Entries");
  h_1stripCharge->SetStats(0);
  h_1stripCharge->Draw("HITS");
  c1.Print(plotDir + "oneStripCharge.png");
  c1.Clear();
  h_1stripChargeMu->GetXaxis()->SetTitle("Strip charge [fC]");
  h_1stripChargeMu->GetYaxis()->SetTitle("Entries");
  h_1stripChargeMu->SetStats(0);
  h_1stripChargeMu->Draw("HITS");
  c1.Print(plotDir + "oneStripCharge_selection.png");
  c1.Clear();
  h_2stripCharge->GetXaxis()->SetTitle("Total charge [fC]");
  h_2stripCharge->GetYaxis()->SetTitle("Entries");
  h_2stripCharge->SetStats(0);
  h_2stripCharge->Draw("HITS");
  c1.Print(plotDir + "twoStripCharge.png");
  c1.Clear();
  h_2stripChargeMu->GetXaxis()->SetTitle("Total charge [fC]");
  h_2stripChargeMu->GetYaxis()->SetTitle("Entries");
  h_2stripChargeMu->SetStats(0);
  h_2stripChargeMu->Draw("HITS");
  c1.Print(plotDir + "twoStripCharge_selection.png");

  c1.Clear();
  h_1stripCl->GetXaxis()->SetTitle("Sector + 0.1*layer");
  h_1stripCl->GetYaxis()->SetTitle("Strip number");
  h_1stripCl->SetStats(0);
  h_1stripCl->Draw("colz");
  c1.Print(plotDir + "oneStripCluster.png");
  c1.Clear();
  h_1stripClMu->GetXaxis()->SetTitle("Sector + 0.1*layer");
  h_1stripClMu->GetYaxis()->SetTitle("Strip number");
  h_1stripClMu->SetStats(0);
  h_1stripClMu->Draw("colz");
  c1.Print(plotDir + "oneStripCluster_selection.png");

  c1.Clear();
  h_2stripCl->GetXaxis()->SetTitle("Sector + 0.1*layer");
  h_2stripCl->GetYaxis()->SetTitle("Strip number");
  h_2stripCl->SetStats(0);
  h_2stripCl->Draw("colz");
  c1.Print(plotDir + "twoStripCluster.png");
  c1.Clear();
  h_2stripClMu->GetXaxis()->SetTitle("Sector + 0.1*layer");
  h_2stripClMu->GetYaxis()->SetTitle("Strip number");
  h_2stripClMu->SetStats(0);
  h_2stripClMu->Draw("colz");
  c1.Print(plotDir + "twoStripCluster_selection.png");

  c1.Clear();
  h_nhole->GetXaxis()->SetTitle("Number of holes per cluster");
  h_nhole->GetYaxis()->SetTitle("Entries");
  h_nhole->SetStats(0);
  h_nhole->Draw("HITS");
  c1.Print(plotDir + "numberHoleInCluster.png");
  c1.Clear();
  h_nholeMu->GetXaxis()->SetTitle("Number of holes per cluster");
  h_nholeMu->GetYaxis()->SetTitle("Entries");
  h_nholeMu->SetStats(0);
  h_nholeMu->Draw("HITS");
  c1.Print(plotDir + "numberHoleInCluster_selection.png");

  for (size_t i = 0; i < nClHisto; ++i) {
    TString histFilename = "clusterSizeTheta_" + clHistoName[i] + ".png";
    // Plotting 2D histos
    if (i < 2) {
      c1.Clear();
      v_clHistos[i]->GetXaxis()->SetTitle("Muon theta [deg]");
      v_clHistos[i]->GetYaxis()->SetTitle("Strip cluster size");
      v_clHistos[i]->SetStats(0);
      v_clHistos[i]->Draw("colz");
      c1.Print(plotDir + histFilename);
    }
    // Projection of cluster size histograms
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.12);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
    TH1D* h_avgClusterSize = v_clHistos[i]->ProfileX("profX", 0, -1, "");
    h_avgClusterSize->GetXaxis()->SetTitle("Muon theta [deg]");
    h_avgClusterSize->GetYaxis()->SetTitle("Average cluster size");
    h_avgClusterSize->SetStats(0);
    h_avgClusterSize->SetMinimum(3.0);
    h_avgClusterSize->SetMaximum(7.0);
    h_avgClusterSize->Draw("");
    c1.Print(plotDir + "avg_" + histFilename);
  }

  for (size_t i = 0; i < nLayer; ++i) {
    HistoCl& layHisto = v_layerHistos.at(i);
    TString histFilename = TString::Format("clusterSizeThetaPerLayer_stIndex%d_stPhi%d_mlt%d_gasgap%d",
                                           layHisto.stationIndex,
                                           layHisto.stationPhi,
                                           layHisto.multiplet,
                                           layHisto.gasgap);
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetLeftMargin(0.12);
    c1.SetTopMargin(0.02);
    c1.SetBottomMargin(0.08);
    TH1D* h_avgClusterSize = layHisto.h2f->ProfileX("profX", 0, -1, "");
    h_avgClusterSize->GetXaxis()->SetTitle("Muon theta [deg]");
    TString ylabel = TString::Format("Average cluster size of sector %d layer %d",
                     sector(layHisto.stationIndex, layHisto.stationPhi),
                     layer(layHisto.multiplet, layHisto.gasgap));
    h_avgClusterSize->GetYaxis()->SetTitle(ylabel);
    h_avgClusterSize->SetStats(0);
    h_avgClusterSize->SetMinimum(3.0);
    h_avgClusterSize->SetMaximum(7.0);
    h_avgClusterSize->Draw("");
    c1.Print(plotDir + "layer_" + histFilename + ".png");
  }

  delete h_1stripCl; delete h_1stripClMu;
  delete h_2stripCl; delete h_2stripClMu;
  delete h_1stripCharge; delete h_1stripChargeMu;
  delete h_nhole; delete h_nholeMu;
  for (auto hist: v_clHistos) {delete hist;}
  return 0;
}
