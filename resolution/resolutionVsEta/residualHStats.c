
struct GaussParam {
  float m_p0{0.0};
  float m_mu{0.0};
  float m_sigma{0.0};
  float m_xmin{0.0};
  float m_xmax{0.0};

  GaussParam() {}
  GaussParam(float p0, float mu, float sig, float min, float max) 
    : m_p0(p0), m_mu(mu), m_sigma(sig), m_xmin(min), m_xmax(max)
  { 
  }
};

int residualHStats() {

  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  //gStyle->SetOptStat(0);

  TString outputDir = "plots/";
  TString inDatasetPath = "/eos/atlas/atlastier0/rucio/data23_13p6TeV/physics_Main/00454222/data23_13p6TeV.00454222.physics_Main.merge.NTUP_MUTEST.f1360_m2180_c1404_m2100/";
  TString datasetListName = "ds_454222.txt";
  std::ifstream inDatasetFile;
  inDatasetFile.open(datasetListName);
  std::vector<TString> datasetList;

  if (inDatasetFile.is_open()) {
    std::string line;
    while (std::getline(inDatasetFile, line)) {
      if (line[0] == '#') continue;
      datasetList.push_back(line.c_str());
    }
    inDatasetFile.close(); // CLose input file
  }
  std::cout << "...Reading the following root files" << std::endl;
  for (auto f: datasetList) {
    std::cout << f << std::endl;
  }

  TChain* fChain = new TChain("BasicTesterTree");
  //std::cout << "...Processing the following root files" << std::endl;
  for (auto ds: datasetList) {
    TString filename = inDatasetPath + ds;
    fChain->Add(filename);
    //std::cout << filename << std::endl;
  }
  TTreeReader reader;
  reader.SetTree(fChain);

  TTreeReaderArray<float> muons_pt   (reader, "muons_pt");
  TTreeReaderArray<float> muons_eta  (reader, "muons_eta");
  TTreeReaderArray<float> muons_phi  (reader, "muons_phi");
  TTreeReaderArray<float> muons_e    (reader, "muons_e");
  TTreeReaderArray<int> muons_q      (reader, "muons_q");
  TTreeReaderArray<unsigned short> muons_author  (reader, "muons_author");
  TTreeReaderArray<unsigned short> muons_type    (reader, "muons_type");
  //
  TTreeReaderArray<unsigned short> stgcOnTrack_MuonLink      (reader, "stgcOnTrack_MuonLink");
  TTreeReaderArray<unsigned short> stgcOnTrack_channel      (reader, "stgcOnTrack_channel");
  TTreeReaderArray<unsigned char> stgcOnTrack_channel_type      (reader, "stgcOnTrack_channel_type");
  TTreeReaderArray<unsigned char> stgcOnTrack_gas_gap      (reader, "stgcOnTrack_gas_gap");
  TTreeReaderArray<unsigned char> stgcOnTrack_multiplet      (reader, "stgcOnTrack_multiplet");
  TTreeReaderArray<char> stgcOnTrack_stationEta      (reader, "stgcOnTrack_stationEta");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationIndex      (reader, "stgcOnTrack_stationIndex");
  TTreeReaderArray<unsigned char> stgcOnTrack_stationPhi      (reader, "stgcOnTrack_stationPhi");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_x      (reader, "stgcOnTrackGlobalPos_x");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_y      (reader, "stgcOnTrackGlobalPos_y");
  TTreeReaderArray<float> stgcOnTrackGlobalPos_z      (reader, "stgcOnTrackGlobalPos_z");
  TTreeReaderArray<int>   stgcOnTrackNStrips       (reader, "stgcOnTrackNStrips");
  TTreeReaderArray<float> stgcOnTrackPullTrack     (reader, "stgcOnTrackPullTrack");
  TTreeReaderArray<float> stgcOnTrackPullTruth     (reader, "stgcOnTrackPullTruth");
  TTreeReaderArray<float> stgcOnTrackResidualTrack (reader, "stgcOnTrackResidualTrack");
  TTreeReaderArray<float> stgcOnTrackResidualTruth (reader, "stgcOnTrackResidualTruth");
  TTreeReaderArray<float> stgcOnTrackLocalPos_x     (reader, "stgcOnTrackLocalPos_x");
  TTreeReaderArray<float> stgcOnTrackLocalPos_y     (reader, "stgcOnTrackLocalPos_y");
  TTreeReaderArray<float> stgcOnTrackError         (reader, "stgcOnTrackError");

  std::cout << "... done reading file" << std::endl;

  TH1F* h_residualTrackPad = new TH1F("residualTrackPad", "", 120, -300, 300);
  TH1F* h_residualTrackStrip = new TH1F("residualTrackStrip", "", 120, -1.5, 1.5);
  TH1F* h_residualTrackStrip1 = new TH1F("residualTrackStrip1", "", 120, -1.5, 1.5);
  TH1F* h_residualTrackWire = new TH1F("residualTrackWire", "", 60, -30, 30);
  
  const float thetaStep = 2.0;
  const float thetaMax{35.0}, thetaMin{5.0};
  const int nTHETA = static_cast<int>((thetaMax - thetaMin) / thetaStep);

  // Container for histos of residuals vs theta. 
  // 1st element of the array: inclusive residuals, 2nd element: large sector, 3rd element: small sector
  std::array<std::vector<TH1F*>, 3> hv_residualEtaTrack{{std::vector<TH1F*>(nTHETA),std::vector<TH1F*>(nTHETA),std::vector<TH1F*>(nTHETA)}};
  const float XMIN{-2.0}, XMAX{2.0};
  const int NBINS = static_cast<int>((XMAX - XMIN) / 0.05);
  for (int i = 0; i < 3; ++i) {
    TString prefix = "";
    switch (i) {
      case 1:
        prefix = "STL";
        break;
      case 2:
        prefix = "STS";
        break;
    }

    for (int k = 0; k < nTHETA; ++k) {
      TString hname = prefix + TString::Format("residualEta_%d", k);
      hv_residualEtaTrack[i].at(k) = new TH1F(hname, "", NBINS, XMIN, XMAX);
    }
  }
  std::array<std::vector<float>, 3> residualEta_x;
  std::array<std::vector<float>, 3> residualEta_y;
  std::array<std::vector<float>, 3> residualEta_ex;
  std::array<std::vector<float>, 3> residualEta_ey;

  std::cout  << "...starting loop" << std::endl;
  int nEvent{0};
  while (reader.Next()) {
    int nResTrack = stgcOnTrackResidualTrack.GetSize();
    //std::cout << "n_elements: " << nResTrack << std::endl;
    for (int i = 0; i < nResTrack; ++i) {
        int channelType = stgcOnTrack_channel_type[i];
        int stationIndex = static_cast<int>(stgcOnTrack_stationIndex[i]);
        float muPt = muons_pt[stgcOnTrack_MuonLink[i]];
        int muAuthor = static_cast<int>(muons_author[stgcOnTrack_MuonLink[i]]); 
        int muType = static_cast<int>(muons_type[stgcOnTrack_MuonLink[i]]); 
        float eta = muons_eta[stgcOnTrack_MuonLink[i]];
        float theta = 2 * TMath::ATan(TMath::Exp(-eta)) * 180.0 / TMath::Pi();

        //std::cout << "channelType: " << channelType << std::endl;
        if (channelType == 0) {
          h_residualTrackPad->Fill(stgcOnTrackResidualTrack[i] );
        } else if (channelType == 1) {
          h_residualTrackStrip->Fill(stgcOnTrackResidualTrack[i] );

          //int otNStrips = stgcOnTrackNStrips[i];
          if (muPt>15.0 && ((muAuthor==1) || (muAuthor==5))) {
            h_residualTrackStrip1->Fill(stgcOnTrackResidualTrack[i] );

            // Determine the eta bin
            if (theta < 0.) {
              std::cout << "Got negative theta: " << theta << std::endl;
              continue;
            }
            if ((theta > thetaMax) || (theta < thetaMin)) continue;
            int k = TMath::Floor((theta - thetaMin) / thetaStep);
            hv_residualEtaTrack[0].at(k)->Fill(stgcOnTrackResidualTrack[i]);
            switch (stationIndex) {
              case 58:
                hv_residualEtaTrack[1].at(k)->Fill(stgcOnTrackResidualTrack[i]);
                break;
              case 57:
                hv_residualEtaTrack[2].at(k)->Fill(stgcOnTrackResidualTrack[i]);
                break;
            }
          }
        } else if (channelType == 2) {
          h_residualTrackWire->Fill( stgcOnTrackResidualTrack[i] );
        } else {
          std::cout << "Wrong channel type: " << channelType << std::endl;
        }
    }
    ++nEvent;
  }
  std::cout  << "...Processed " << nEvent << " events" << std::endl;

  TCanvas c1("c1", "residual", 800, 600);
  h_residualTrackPad->GetXaxis()->SetTitle("Pad Residual [mm]");
  h_residualTrackPad->GetYaxis()->SetTitle("Entries");
  h_residualTrackPad->Draw("HIST");
  c1.Print(outputDir + "graph_residualTrack_pad.png");
  //
  c1.Clear();
  h_residualTrackStrip->GetXaxis()->SetTitle("Strip Residual [mm]");
  h_residualTrackStrip->GetYaxis()->SetTitle("Entries");
  h_residualTrackStrip->Draw("HIST");
  c1.Print(outputDir + "graph_residualTrack_strip.png");
  //
  c1.Clear();
  h_residualTrackStrip1->GetXaxis()->SetTitle("Strip Residual [mm]");
  h_residualTrackStrip1->GetYaxis()->SetTitle("Entries");
  h_residualTrackStrip1->Draw("HIST");
  c1.Print(outputDir + "graph_residualTrack_strip_filter.png");
  //
  c1.Clear();
  h_residualTrackWire->GetXaxis()->SetTitle("Wire Residual [mm]");
  h_residualTrackWire->GetYaxis()->SetTitle("Entries");
  h_residualTrackWire->Draw("HIST");
  c1.Print(outputDir + "graph_residualTrack_Wire.png");

  // fit histos of residual vs eta
  std::cout << "\n...Computing track residuals" << std::endl;
  for (int i = 0; i < 3; ++i) {
    TString fname = outputDir + "graph_residualEtaTrack";
    switch (i) {
      case 1:
        fname = fname + "STL";
        break;
      case 2:
        fname = fname + "STS";
        break;
    }
    for (int k = 0; k < nTHETA; ++k) {
      if (!hv_residualEtaTrack[i].at(k)) continue;
      if (hv_residualEtaTrack[i].at(k)->GetEntries() < 400) continue;

      c1.Clear();
      c1.SetRightMargin(0.02);
      c1.SetTopMargin(0.02);
      hv_residualEtaTrack[i].at(k)->GetXaxis()->SetTitle("Residual [mm]");
      hv_residualEtaTrack[i].at(k)->GetYaxis()->SetTitle("Entries");

      float rmsTk = hv_residualEtaTrack[i].at(k)->GetRMS();
      float meanTk = hv_residualEtaTrack[i].at(k)->GetMean();
      TF1 *g1 = new TF1("g1", "gaus", meanTk-1.0*rmsTk, meanTk+1.0*rmsTk);
      TF1 *g2 = new TF1("g2", "gaus", XMIN, XMAX);
      TF1 *totalGaus = new TF1("totalGaus", "gaus(x,[0],[1],[2])+gaus(x,[3],[1],[4])", XMIN, XMAX);
      hv_residualEtaTrack[i].at(k)->Fit(g1, "R");
      hv_residualEtaTrack[i].at(k)->Fit(g2, "R");
      double fitpar[6];
      g1->GetParameters(&fitpar[0]);
      g2->GetParameters(&fitpar[3]);
      totalGaus->SetParameters(fitpar[0],fitpar[1],fitpar[2],fitpar[3],fitpar[5]);
      TFitResultPtr resTk = hv_residualEtaTrack[i].at(k)->Fit(totalGaus, "S", "", XMIN, XMAX);
      TString nameTag = TString::Format("_%03.1f", (k*thetaStep + thetaMin) + thetaStep/2);
      c1.Print(fname + nameTag + ".png");

      float sigmaTk = resTk->Parameter(2);
      float eSigmaTk = resTk->ParError(2);
      residualEta_x[i].push_back(k * thetaStep + thetaMin + thetaStep/2);
      residualEta_y[i].push_back(sigmaTk);
      residualEta_ex[i].push_back(thetaStep/2);
      residualEta_ey[i].push_back(eSigmaTk);
    }
  }
  std::cout << "\n...Done computing residuals" << std::endl;

  // Plot histos
  TGraphErrors* h_residualVsEta_track[3] = {nullptr, nullptr, nullptr};
  for (int i = 0; i < 3; ++i) {
    c1.Clear();
    c1.SetRightMargin(0.02);
    c1.SetTopMargin(0.02);
    const int nPoint = residualEta_x[i].size();
    h_residualVsEta_track[i] = new TGraphErrors(nPoint,
                                                residualEta_x[i].data(),
                                                residualEta_y[i].data(),
                                                residualEta_ex[i].data(),
                                                residualEta_ey[i].data());
    h_residualVsEta_track[i]->SetTitle("");;
    h_residualVsEta_track[i]->SetMarkerStyle(23);
    h_residualVsEta_track[i]->SetMarkerSize(1.5);
    h_residualVsEta_track[i]->GetXaxis()->SetTitle("Muon incident angle #theta [deg]");
    h_residualVsEta_track[i]->GetYaxis()->SetTitle("#sigma_{sTGC} [mm]");;
    h_residualVsEta_track[i]->SetMarkerColor(1);
    h_residualVsEta_track[i]->SetLineColor(1);
    h_residualVsEta_track[i]->Draw("AP SAME");

    TString fname = "graph_residualVsTheta_track";
    switch (i) {
      case 0:
        fname = fname + "_all";
        break;
      case 1:
        fname = fname + "_STL";
        break;
      case 2:
        fname = fname + "_STS";
        break;
    }
    c1.Print(outputDir + fname + ".png");
  }

  gStyle->SetOptStat(0);
  c1.Clear();
  c1.SetRightMargin(0.02);
  c1.SetTopMargin(0.02);
  TH1F h_tmp("res", "", 8, 0, 40);
  //TGraph h_tmp;
  h_tmp.SetMaximum(0.25);
  h_tmp.SetMinimum(0.10);
  h_tmp.SetTitle("");
  //h_tmp.SetOptStat(0);
  h_tmp.GetXaxis()->SetTitle("Muon incident angle #theta [deg]");
  h_tmp.GetYaxis()->SetTitle("#sigma_{sTGC} [mm]");
  h_tmp.Draw();
  h_residualVsEta_track[0]->SetMarkerStyle(20);
  h_residualVsEta_track[0]->SetMarkerColor(1);
  h_residualVsEta_track[0]->SetLineColor(1);
  h_residualVsEta_track[0]->SetMarkerSize(1.2);
  h_residualVsEta_track[0]->Draw("P SAME");
  //
  h_residualVsEta_track[1]->SetMarkerStyle(22);
  h_residualVsEta_track[1]->SetMarkerColor(2);
  h_residualVsEta_track[1]->SetLineColor(2);
  h_residualVsEta_track[1]->SetMarkerSize(1.2);
  h_residualVsEta_track[1]->Draw("P SAME");
  //
  h_residualVsEta_track[2]->SetMarkerStyle(23);
  h_residualVsEta_track[2]->SetMarkerColor(4);
  h_residualVsEta_track[2]->SetLineColor(4);
  h_residualVsEta_track[2]->SetMarkerSize(1.2);
  h_residualVsEta_track[2]->Draw("P SAME");
  //
  TLegend leg(0.20, 0.8, 0.46, 0.94);
  leg.AddEntry(h_residualVsEta_track[0], "All sectors");
  leg.AddEntry(h_residualVsEta_track[1], "Large sectors");
  leg.AddEntry(h_residualVsEta_track[2], "Small sectors");
  leg.Draw();
  c1.Print(outputDir + "graph_residualVsTheta_comparison.png");

  //delete file; delete tree;
  delete h_residualTrackPad; delete h_residualTrackStrip; delete h_residualTrackWire;
  for (int i = 0; i < 3; ++i) {
    for (int k = 0; k < nTHETA; ++k) {delete hv_residualEtaTrack[i].at(k);}
    delete h_residualVsEta_track[i];
  }
  return 0;
}
